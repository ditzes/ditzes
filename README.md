## ditzes

Offline-first HN client with comments flatly sorted by time instead of points

Runs in the following ways:

- Online service at ditzes.com
- Self-hosted service via `ditzes-api` binary (https://codeberg.org/ditzes/ditzes/releases)
- Tauri Desktop application

Only the two last offer you the offline-first experience, as the online service requires internet connectivity to access.
