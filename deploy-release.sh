#! /usr/bin/env sh

set -e

USER=root
HOST=ditzes.com

if [[ $# -eq 0 ]] ; then
    echo "Missing \$COMMIT as first argument to deploy"
    exit 1
fi

COMMIT=$1

log () {
  echo "############"
  echo "#### $1"
  echo "############"
}

WORKDIR=$(mktemp -d)

log "Fetching release info for commit $COMMIT"

RELEASE=$(curl -X 'GET' \
  "https://codeberg.org/api/v1/repos/ditzes/ditzes/releases/tags/$COMMIT" \
  -H 'accept: application/json')

TAR_URL=$(echo $RELEASE | jq -r '.assets[0].browser_download_url')
TAR_NAME=$(echo $RELEASE | jq -r '.assets[0].name')

log "Entering WORKDIR=$WORKDIR"
cd $WORKDIR

log "Downloading release"
wget $TAR_URL -O $TAR_NAME

log "Extracting"
tar xfv $TAR_NAME --strip-components=1

log "Deploying binaries"
scp ditzes-api $USER@$HOST:/root/ditzes-api-new
scp ditzes-cli $USER@$HOST:/root/ditzes-cli

log "Restarting service"
ssh $USER@$HOST "systemctl stop ditzes && mv /root/ditzes-api /root/ditzes-api-old && mv /root/ditzes-api-new /root/ditzes-api && systemctl start ditzes && sleep 1 && systemctl status ditzes"
