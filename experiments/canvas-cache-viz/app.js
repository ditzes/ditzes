// window.CP.PenTimer.MAX_TIME_IN_LOOP_WO_EXIT = 6000;
console.clear()
// var boxes = 30209148
// var boxes = 10000000
var boxes = 1000000
var boxesPerLine = 2000

// TODO try to first figure out the color of each element, then render by color instead of by x/y
// TODO try to batch things into requestAnimationFrame so the state would progressively render instead of all at once
// TODO try to use OffscreenCanvas in a worker (or many?) instead
// TODO try using direct ImageData https://stackoverflow.com/a/58485681

const color = () => {
  if (Math.random() > 0.5) {
    return 'red'
  } else {
    return 'green'
  }
}
var canvas = document.getElementById('canvas');
var $time = document.getElementById('time');
// var ctx = canvas.getContext('2d', { alpha: false });
var ctx = canvas.getContext('2d', {alpha: false, desynchronized: true});
ctx.scale(0.5, 0.5)

// var offscreenCanvas = document.createElement('canvas');
// offscreenCanvas.width = canvas.width;
// offscreenCanvas.height = canvas.height;
// var offscreenCtx = offscreenCanvas.getContext('2d', { alpha: false });

// ctx.scale(0.1, 0.1)

var boxSize = 1

function draw() {
  $time.innerText = "Measuring..."
  const start_draw = performance.now();
  let row = boxSize;
  let xPos = 0;
  // ctx.beginPath();
  let lastColor = color();
  ctx.fillStyle = lastColor;

  // First, figure out which element has what color, and draw based on that.
  let redItems = []
  let greenItems = []
  for (var x = 0; x < boxes; x++) {
    if (x != 0 && x % boxesPerLine === 0) {
      row = row + boxSize
      xPos = 0
    }
    xPos = xPos + boxSize;
    // setTimeout(() => {
    let newColor = color();
    if (newColor != lastColor) {
      lastColor = newColor
    }
    if (lastColor === 'green') {
      greenItems.push({x: xPos, y: row})
    } else {
      redItems.push({x: xPos, y: row})
    }
    // }, 0)
  }
  row = boxSize;
  xPos = 0;

  ctx.fillStyle = 'red'
  for (item of redItems) {
    ctx.fillRect(item.x, item.y, boxSize, boxSize)
  }

  ctx.fillStyle = 'green'
  for (item of greenItems) {
    ctx.fillRect(item.x, item.y, boxSize, boxSize)
  }

  // for (var x = 0; x < boxes; x++) {
  //   if (x != 0 && x % boxesPerLine === 0) {
  //     row = row + boxSize
  //     xPos = 0
  //   }
  //   xPos = xPos + boxSize;
  //   // setTimeout(() => {
  //   let newColor = color();
  //   if (newColor != lastColor) {
  //     lastColor = newColor
  //     ctx.fillStyle = lastColor;
  //   }
  //   ctx.fillRect(xPos, row, boxSize, boxSize);
  //   
  //   // }, 0)
  // }

  // ctx.closePath();
  // ctx.drawImage(offscreenCanvas, 0, 0);
  const end_draw = performance.now();
  $time.innerText = `${end_draw - start_draw}ms`
  console.log($time.innerText)
}
window.onload = draw
