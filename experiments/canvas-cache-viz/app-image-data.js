// window.CP.PenTimer.MAX_TIME_IN_LOOP_WO_EXIT = 6000;
console.clear()
var boxes = 30219699
// var boxes = 10000000
// var boxes = 6000000

// var boxes = 1000000
// var boxesPerLine = 2000

const color = () => {
  if (Math.random() > 0.5) {
    return 'red'
  } else {
    return 'green'
  }
}
var canvas = document.getElementById('canvas');
var $time = document.getElementById('time');
var ctx = canvas.getContext('2d', {alpha: true, desynchronized: true});
ctx.scale(0.01, 0.01)

function draw() {
  $time.innerText = "Measuring..."
  const start_draw = performance.now();

  let lastColor = color();
  ctx.fillStyle = lastColor;

  var imageData=ctx.createImageData(2000, 20000);
  var buf=new Uint32Array(imageData.data.buffer);


  for (var x = 0; x < boxes; x++) {
    buf[x]=color()=="red"?0xFF0000FF:0xFF00FF00;
  }

  ctx.putImageData(imageData, 0, 0);

  const end_draw = performance.now();
  $time.innerText = `${end_draw - start_draw}ms`
  console.log($time.innerText)
}
window.onload = draw
