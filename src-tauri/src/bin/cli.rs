use clap::Parser;
use ditzes::{article, build_info, cache, hn, logs, search, settings, settings::Settings};
use std::{time, time::Instant};
// use tokio::sync::watch;
use ditzes::update_service::UpdateService;
use futures::future;
use std::{collections::HashMap, thread};

/// Simple program to greet a person
#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
struct Args {
  /// Cmd to run
  cmd: Option<String>,

  /// Where to start
  #[clap(short, long)]
  from: Option<u32>,

  /// Where to end
  #[clap(short, long)]
  to: Option<u32>,

  /// What ID to use
  #[clap(short, long)]
  id: Option<u32>,

  /// How many items to fetch when `latest_items` is called
  #[clap(short, long)]
  n: Option<u32>,

  #[clap(short, long)]
  article_url: Option<String>,

  /// What list to use when `fetch_list` called
  #[clap(short, long)]
  list_name: Option<String>,

  /// What search term to use when `query-search-index` is called
  #[clap(short, long)]
  query: Option<String>,

  /// Which user to use for the user-related queries
  #[clap(short, long)]
  username: Option<String>,
}

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
  hn::install_data_dir();

  let args = Args::parse();

  println!("{:?}", args);

  actix_web::rt::System::with_tokio_rt(|| {
    tokio::runtime::Builder::new_multi_thread()
      .enable_all()
      .worker_threads(12)
      .thread_name("main-tokio")
      .build()
      .unwrap()
  })
  .block_on(async_main(args));

  Ok(())
}

use async_recursion::async_recursion;

#[async_recursion]
async fn get_submission_title(item: &hn::Item) -> String {
  match item.parent {
    Some(parent_id) => {
      let parent_item = hn::get_item(false, parent_id).await.unwrap().clone();
      get_submission_title(&parent_item).await
    }
    None => item.title.as_ref().unwrap().to_string(),
  }
}

fn truncate(s: &str, max_chars: usize) -> &str {
  match s.char_indices().nth(max_chars) {
    None => s,
    Some((idx, _)) => &s[..idx],
  }
}

async fn replies(_settings: Settings, args: Args) {
  let username = args.username.expect("You need to provide --user argument");
  println!("{}", username);
  let mut res = hn::get_replies_to_user_submitted(username).await.unwrap();
  println!("Found the following replies ({}):", res.len());
  println!("{:#?}", res);

  res.sort_by(|a, b| a.time.cmp(&b.time));

  for item in res {
    let deleted = match item.deleted {
      Some(deleted) => deleted,
      None => false,
    };
    if !deleted {
      let parent_title = get_submission_title(&item).await;
      println!("###############");
      println!(
        "[{}] In {:?} by {:?} ({})",
        item.human_time.unwrap(),
        truncate(&parent_title, 30),
        item.by.unwrap(),
        format!("https://news.ycombinator.com/item?id={}", item.id)
      );
      match item.text {
        Some(text) => {
          println!("{:#?}", text);
        }
        None => {
          println!("[Item has no text!]")
        }
      }
      println!("");
    }
  }
}

async fn latest_items(_settings: Settings, args: Args) {
  args
    .n
    .expect("You need to set `n` to number of items to fetch");
  let items = hn::get_latest_items(true, args.n.unwrap()).await.unwrap();
  println!("Retrieved {:?} items", items.len());
}

// async fn comments(_settings: Settings, args: Args) {
//   let (tx, rx) = watch::channel((0, 0));
//
//   let handler = thread::spawn(move || {
//     loop {
//       let res = *rx.borrow();
//       let (current, total) = res;
//       println!("{:?}/{:?}", current, total);
//       thread::sleep(time::Duration::from_millis(250));
//       if current == total {
//           break;
//       } else {
//           thread::sleep(time::Duration::from_secs(1));
//       }
//     }
//   });
//
//   hn::get_items_with_progress(tx, args.id.unwrap()).await;
//
//   handler.join().unwrap();
// }

async fn fetch(_settings: Settings, args: Args) {
  hn::get_items_between(false, args.from.unwrap(), args.to.unwrap()).await;
}

async fn get_article(args: Args) {
  article::get_article(args.article_url.unwrap());
}

async fn task_get_article(url: String) -> String {
  println!("Fetching article {:?}", url);
  article::get_article(url)
}

async fn get_frontpage_articles(settings: Settings, _args: Args) {
  let mut articles: HashMap<u32, String> = HashMap::new();
  let frontpage = hn::get_list_by_name(settings, "frontpage".to_string())
    .await
    .unwrap();

  // for item in frontpage.items {
  //     let url = item.url.unwrap().clone();
  //     println!("Fetching article {:?}", url);
  //     articles.insert(item.id, article::get_article(url));
  // }
  //
  let items_to_fetch = frontpage.items.to_vec();

  let tasks: Vec<_> = items_to_fetch
    .iter()
    .take(25)
    .map(move |item| {
      let item = item.clone();
      tokio::spawn(async move {
        let url = match item.url {
          Some(body) => body,
          None => "".to_string(),
        };
        let article_body = task_get_article(url).await;
        // let article_body = task_get_article(item.url.unwrap()).await;
        // articles.insert(item.id, article_body);
        (item.id, article_body)
      })
    })
    .collect();
  let items = future::join_all(tasks).await;

  for res in items {
    let (item_id, article_body) = res.unwrap();
    // let (item_id, article_body) = match res {
    //     Ok(tuple) => tuple,
    //     Err(e) => println!("Err: {:?}", e),
    // };

    // match res {
    //     Some(item) => articles.insert(item.0, item.1),
    //     None => println!("None"),
    // }
    articles.insert(item_id, article_body);
    // match item {
    //   Ok(i) => articles.insert(item.id, i),
    //   Err(e) => println!("Error: {}", e),
    // }
  }

  println!("{:#?}", articles);
}

async fn fetch_list(settings: Settings, args: Args) {
  println!("Using the following settings");
  println!("{:#?}", settings);

  let list_name = match args.list_name {
    Some(name) => name,
    None => panic!("No `list-name` provided, which is required to use the `fetch_list` command"),
  };
  println!("Getting list {:?}", list_name);
  let list = match hn::get_list_by_name(settings, list_name).await {
    Some(list) => list,
    None => panic!("Something went wrong "),
  };

  println!("Got list with {} items", list.items.len());

  for item in list.items {
    println!("Title: {}", item.title.unwrap());
  }
}

async fn list_versions(settings: Settings, args: Args) {
  let list_name = match args.list_name {
    Some(name) => name,
    None => panic!("No `list-name` provided, which is required to use the `fetch_list` command"),
  };
  let versions = hn::get_list_versions(settings, list_name).await.unwrap();
  println!("{:#?}", versions);
}

async fn generate_cache_image(_settings: settings::Settings, args: Args) {
  cache::generate_cache_image(args.from.unwrap(), args.to.unwrap()).await;
}

async fn build_search_index(settings: settings::Settings, _args: Args) {
  println!("Indexing");
  let res = search::create_index(&settings).await.unwrap();
  println!("DocStamp at {:?}", res);
}

async fn query_search_index(settings: settings::Settings, args: Args) {
  let query_term = args.query.unwrap();
  let search_results = search::query_index(&settings, query_term).await.unwrap();
  for result in search_results {
    println!("## SearchResult => ");
    println!("{:#?}", result);
  }
}

async fn auto_update(settings: settings::Settings, _args: Args) {
  let update_service: UpdateService = UpdateService::new(&settings).await;

  println!("Starting UpdateService");
  // let handler = thread::spawn(move || {
  update_service.start().await;
  // });

  println!("Updating UpdateService UpdateInterval in 3 seconds");
  thread::sleep(time::Duration::from_secs(3));
  update_service.set_update_interval(3).await;

  println!("Killing UpdateService in 60 seconds");
  thread::sleep(time::Duration::from_secs(60));
  update_service.stop().await;

  // handler.join().unwrap();
  println!("Blocking for three seconds...");
  thread::sleep(time::Duration::from_secs(3));

  // println!("Blocking indefinitly, press CTRL+C to exit");
  // loop {
  //     thread::sleep(time::Duration::from_secs(1000));
  // }
}

async fn load_many_comments(_settings: settings::Settings, _args: Args) {
  let items = hn::get_items(30381000).await.unwrap();
  println!("Fetched {:?} items", items.len())
}

async fn build_info(_settings: settings::Settings, _args: Args) {
  let build_info = build_info::new();
  println!("{:#?}", build_info)
}

async fn async_main(args: Args) {
  logs::init();

  let now = Instant::now();
  let cmd = &args.cmd.as_ref().unwrap();
  let cmd_name = cmd.as_str();

  let settings = settings::load_or_setup_settings();

  match cmd_name {
    "build-info" => build_info(settings, args).await,
    "latest-items" => latest_items(settings, args).await,
    // "comments" => comments(settings, args).await,
    "fetch" => fetch(settings, args).await,
    "generate-cache-image" => generate_cache_image(settings, args).await,
    "fetch-list" => fetch_list(settings, args).await,
    "list-versions" => list_versions(settings, args).await,
    "get-article" => get_article(args).await,
    "get-frontpage-articles" => get_frontpage_articles(settings, args).await,
    "build-search-index" => build_search_index(settings, args).await,
    "query-search-index" => query_search_index(settings, args).await,
    "auto-update" => auto_update(settings, args).await,
    "load-many-comments" => load_many_comments(settings, args).await,
    "replies" => replies(settings, args).await,
    _ => println!("No cmd found"),
  }
  // hn::get_items_between(args.from, args.to).await;
  let elapsed = now.elapsed();
  println!("Command took {:.2?} in total", elapsed);
}
