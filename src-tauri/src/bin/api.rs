use ditzes::api;

fn main() {
  actix_web::rt::System::with_tokio_rt(|| {
    tokio::runtime::Builder::new_multi_thread()
      .enable_all()
      .worker_threads(12)
      .thread_name("main-tokio")
      .build()
      .unwrap()
  })
  .block_on(api::async_main());
}
