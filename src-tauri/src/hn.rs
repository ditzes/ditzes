// use futures::future;
use serde::{Deserialize, Serialize};
// use std::collections::VecDeque;
// use futures::executor;
// use tokio;
use futures::future;
extern crate savefile;
use dirs::data_dir;
// use futures::prelude::*;
use awc::{http::header, Client, Connector};
use mkdirp::mkdirp;
use rustls::{ClientConfig, OwnedTrustAnchor, RootCertStore};
use savefile::prelude::*;
use std::{
  collections::VecDeque,
  path::{Path, PathBuf},
  sync::Arc,
};

use futures::StreamExt;
extern crate chrono;

use chrono::prelude::*;
use url::Url;

use log::{debug, info, warn};

// use tokio::runtime::Builder;

#[derive(Debug, Deserialize, Serialize, Savefile, Clone)]
pub struct Item {
  pub id: u32,
  pub title: Option<String>,
  pub url: Option<String>,
  pub domain: Option<String>,
  pub by: Option<String>,
  pub text: Option<String>,
  pub r#type: String,
  pub deleted: Option<bool>,
  pub descendants: Option<u32>,
  pub kids: Option<Vec<u32>>,
  pub score: Option<u32>,
  pub parent: Option<u32>,
  pub parent_text: Option<String>,
  pub time: u32,
  pub human_time: Option<String>,
}

// Duplicate in settings.rs
pub fn install_data_dir() {
  let dir = data_dir().unwrap();
  let app_name = "ditzes";
  let items_directory = "items";
  let p: PathBuf = [dir, PathBuf::from(app_name), PathBuf::from(items_directory)]
    .iter()
    .collect();
  info!(
    "Using {} as data-dir for items",
    p.clone().into_os_string().into_string().unwrap()
  );
  mkdirp(p).expect("Couldn't create data-dir");
  return;
}

pub fn split_id(id: u32) -> Vec<String> {
  let filename = id.to_string();
  let parts = filename.split("");
  let str_parts: Vec<String> = parts.filter(|s| {
    return s != &""
  }).map(|s| s.to_string()).collect();
  // println!("{:#?}", str_parts);
  return str_parts;
}

pub fn shard_filename(shard_factor: usize, parts: Vec<String>) -> PathBuf {
  // How many parts to use for sharding into directories
  // shard_factor = 1 = "abcde" => "a/bcde"
  // shard_factor = 3 = "abcde" => "a/b/c/de"
  // shard_factor = 4 = "abcde" => "a/b/c/d/e"
  // let shard_factor = 3;
  let mut p = PathBuf::new();
  for index in 0..shard_factor {
      p.push(parts[index].clone());
  }
  let len = parts.len() - shard_factor;
  let rest: Vec<String> = parts.as_slice()[parts.len()-len..].to_vec();
  // println!("#######");
  // println!("PathBuf:");
  // println!("{:#?}", p);
  // println!("Rest:");
  // println!("{:#?}", rest);
  let filename = rest.join("");
  p.push(filename);
  return p
}

#[cfg(test)]
mod tests {
    use crate::hn::{split_id, shard_filename};

    #[test]
    fn test_splits_id() {
      let id = 12345678;
      let parts = split_id(id);
      assert_eq!(parts[0], "1");
      assert_eq!(parts[7], "8");
    }
    #[test]
    fn test_shard_filename() {
      let id = 12345678;
      let parts = split_id(id);
      assert_eq!(parts[0], "1");
      assert_eq!(parts[7], "8");
      println!("#######");
      assert_eq!(shard_filename(3, parts.clone()).into_os_string().into_string().unwrap(), "1/2/3/45678");
      assert_eq!(shard_filename(1, parts.clone()).into_os_string().into_string().unwrap(), "1/2345678");
      assert_eq!(shard_filename(7, parts.clone()).into_os_string().into_string().unwrap(), "1/2/3/4/5/6/7/8");
    }
}

pub fn construct_path(id: u32) -> String {
  let dir = data_dir().unwrap();
  // let filename = id.to_string();
  let app_name = "ditzes";
  let items_directory = "items";
  let parts = split_id(id);
  let sharded_filename = shard_filename(3, parts);

  let p: PathBuf = [
    dir,
    PathBuf::from(app_name),
    PathBuf::from(items_directory),
    sharded_filename,
  ]
  .iter()
  .collect();

  let mut directory = p.clone();
  directory.pop();
  mkdirp(directory).expect("Couldn't create sharded item directory");

  let path_s: String = p.into_os_string().into_string().unwrap();
  // info!("Using data dir {}", path_s);
  return path_s;
}

fn save_item(item: &Item) {
  let filename = construct_path(item.id);
  save_file(&filename, 0, item).unwrap();
}

fn load_item(id: u32) -> Item {
  let filename = construct_path(id);
  load_file(&filename, 0).expect(&format!("Failed to read file {}", id))
}

fn item_exists(id: u32) -> bool {
  let filename = construct_path(id);
  return Path::new(&filename).exists();
}

use cached::proc_macro::cached;
use isahc::prelude::*;

// #[cached(size=13421772, sync_writes = true)]
// #[cached(size=13421772, time=60)]
// #[cached(time=10)]
pub async fn get_item(skip_cache: bool, id: u32) -> Option<Item> {
  // info!("Does item {:?} exists already?", id);
  if !skip_cache && item_exists(id) {
    // info!("Using cached item for {:?}", id);
    return Some(load_item(id));
  }
  info!("Fetching item {:?}", id);

  let url_to_get = format!("https://hacker-news.firebaseio.com/v0/item/{}.json", id);
  // info!("URL {:?}", url_to_get);
  // let client = reqwest::Client::builder()
  //   // .pool_max_idle_per_host(0)
  //   // .pool_idle_timeout(Duration::new(0, 1))
  //   .build()
  //   .unwrap();

  // let res = client.get(url_to_get).send().await.unwrap();
  let mut res = isahc::get_async(url_to_get).await.unwrap();

  match res.json::<Item>().await {
    Ok(mut item) => {
      // Convert UNIX timestamp to our local time for client-side beautification
      let timestamp = i64::from(item.time);
      let naive = NaiveDateTime::from_timestamp(timestamp, 0);
      let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);
      let datetime: DateTime<Local> = DateTime::from(datetime);
      let newdate = datetime.format("%Y-%m-%d %H:%M:%S").to_string();
      item.human_time = Some(newdate);

      // Parse domain from item
      match item.url {
        Some(ref url) => {
          // info!("{:?}", url);
          if url != "" {
            match Url::parse(&url) {
              Ok(parsed_url) => {
                match parsed_url.host_str() {
                  Some(parsed_url) => item.domain = Some(parsed_url.to_string()),
                  None => {
                    println!(
                      "Couldn't get host_str from domain {:#?}, item ID {:?}",
                      &url, item.id
                    );
                  }
                }
                // item.domain = Some(parsed_url.host_str().unwrap().to_string());
              }
              Err(err) => {
                println!(
                  "Couldn't parse domain {:#?} from item ID {:?}",
                  &url, item.id
                );
                println!("{:#?}", err)
              }
            }
          }
        }
        None => {}
      }

      // Skip saving `dead` items, to make sure we persist some useful version of it at least?

      save_item(&item);
      Some(item)
    }
    Err(_) => {
      warn!("Could not download item {:?}", id);
      // Maybe save them as deleted?
      // But then we need to be able to refetch them sometimes as they can
      // get undeleted later
      // let item = Item {
      //   by: None,
      //   deleted: Some(true),
      //   descendants: None,
      //   domain: None,
      //   human_time: None,
      // };
      return None;
    }
  }

  // if item_wrapped == nil {
  //   let item = item.unwrap();
  //   // info!("{:?}", item);
  //   save_item(&item);
  //   item
  // } else {
  //   info!("{} probably deleted", id)
  // }
}

use std::{thread, time};
use tokio::sync::watch::Sender;

// Downloader:

struct Downloader {
  items_to_fetch: Vec<u32>,
  fetched_items: Vec<Item>,
}

impl Downloader {
  fn new() -> Downloader {
    Downloader {
      items_to_fetch: vec![],
      fetched_items: vec![],
    }
  }
  fn add_to_fetch(&mut self, item_to_fetch: u32) {
    self.items_to_fetch.push(item_to_fetch);
  }
}

// use async_channel::{unbounded, TryRecvError};
// pub async fn get_items_with_progress(tx: Sender<(u32, u32)>, start_id: u32) -> Option<Vec<Item>> {
//     let mut current: usize = 0;
//     let mut total: usize = 1;
//
//     let (sender, receiver): (async_channel::Sender<u32>, async_channel::Receiver<u32>) = async_channel::unbounded();
//
//     let r1 = receiver.clone();
//     let r2 = receiver.clone();
//     tokio::spawn(async move {
//         loop {
//             info!("[thread1] Waiting for item IDs to download");
//             let id = r1.recv().await.unwrap();
//             info!("[thread1] Gonna download ID {:?}", id);
//         }
//     });
//
//     tokio::spawn(async move {
//         loop {
//             info!("[thread2] Waiting for item IDs to download");
//             let id = r2.recv().await.unwrap();
//             info!("[thread2] Gonna download ID {:?}", id);
//         }
//     });
//
//     loop {
//         info!("Sending ID");
//         sender.send(start_id).await.unwrap();
//         thread::sleep(time::Duration::from_millis(1000));
//     }
//         // let id = receiver.recv().await.unwrap();
//         // info!("Gonna download ID {:?}", id);
//
//     return None;
//
//     let mut fetched_items: Vec<Item> = vec![];
//     let mut items_to_fetch = VecDeque::new();
//
//     // First item to kickstart the process of fetching them
//     items_to_fetch.push_back(start_id);
//
//     let mut is_first_item = true;
//
//     while let Some(item_to_fetch) = items_to_fetch.pop_front() {
//         let mut skip_cache = false;
//         if is_first_item {
//             skip_cache = true;
//             is_first_item = false;
//         }
//         let mut latest_item = get_item(skip_cache, item_to_fetch).await.unwrap();
//
//         // Fetch all children if there is any
//         if let Some(kids) = &latest_item.kids.clone() {
//             get_vec_of_items(false, kids.to_vec()).await;
//             for &item_id in kids {
//                 total = total + 1;
//                 items_to_fetch.push_back(item_id);
//             }
//         }
//
//         // If there is a parent text, put it in the same item so we can show it
//         // client-side as a quoted text
//         if let Some(parent_id) = latest_item.parent {
//             let possible_parent = fetched_items.iter().position(|r| r.id == parent_id);
//             let parent_text = match possible_parent {
//                 Some(parent_item_index) => {
//                     let parent_item = fetched_items.get(parent_item_index).unwrap();
//                     parent_item.text.clone()
//                 }
//                 None => None,
//             };
//             latest_item.parent_text = parent_text;
//         }
//
//         // Check if the item has `deleted` set as if it does, it's been deleted.
//         // If it doesn't have that set, we know for sure it hasn't been deleted
//         match latest_item.deleted {
//             Some(is_deleted) => {
//                 if !is_deleted {
//                     fetched_items.push(latest_item);
//                     current = current + 1;
//                 } else {
//                     total = total - 1;
//                 }
//             }
//             None => {
//                 fetched_items.push(latest_item);
//                 current = current + 1;
//             }
//         }
//         tx.send((current as u32, total as u32)).unwrap();
//     }
//
//     fetched_items.sort_by(|a, b| a.time.cmp(&b.time));
//
//     Some(fetched_items)
// }

pub async fn get_items(start_id: u32) -> Option<Vec<Item>> {
  let mut fetched_items: Vec<Item> = vec![];
  let mut items_to_fetch = VecDeque::new();

  // First item to kickstart the process of fetching them
  items_to_fetch.push_back(start_id);

  let mut is_first_item = true;

  while let Some(item_to_fetch) = items_to_fetch.pop_front() {
    let mut skip_cache = false;
    if is_first_item {
      skip_cache = true;
      is_first_item = false;
    }
    let mut latest_item = get_item(skip_cache, item_to_fetch).await.unwrap();

    // Fetch all children if there is any
    if let Some(kids) = &latest_item.kids.clone() {
      get_vec_of_items(false, kids.to_vec()).await;
      for &item_id in kids {
        items_to_fetch.push_back(item_id);
      }
    }

    // If there is a parent text, put it in the same item so we can show it
    // client-side as a quoted text
    if let Some(parent_id) = latest_item.parent {
      // let possible_parent = fetched_items.iter().position(|r| r.id == parent_id);
      let possible_parent = get_item(false, parent_id).await;
      let parent_text = match possible_parent {
        Some(parent) => parent.text.clone(),
        None => None,
      };
      latest_item.parent_text = parent_text;
    }

    // Check if the item has `deleted` set as if it does, it's been deleted.
    // If it doesn't have that set, we know for sure it hasn't been deleted
    match latest_item.deleted {
      Some(is_deleted) => {
        if !is_deleted {
          fetched_items.push(latest_item)
        }
      }
      None => fetched_items.push(latest_item),
    }
  }

  fetched_items.sort_by(|a, b| a.time.cmp(&b.time));

  Some(fetched_items)
}

async fn get_list_page(url: String) -> Option<Vec<Item>> {
  // let client = reqwest::Client::builder().build().unwrap();

  // let res = client.get(url).send().await.unwrap();
  let mut res = isahc::get_async(url).await.unwrap();

  let items_ids = res.json::<Vec<u32>>().await.unwrap();

  let tasks: Vec<_> = items_ids
    .iter()
    .take(25)
    .map(|id| {
      let item_id = id.clone();
      tokio::spawn(async move {
        let item = get_item(true, item_id).await.unwrap();
        return item;
      })
    })
    .collect();
  let items = future::join_all(tasks).await;

  let mut fetched_items: Vec<Item> = vec![];

  for item in items {
    match item {
      Ok(i) => fetched_items.push(i),
      Err(e) => warn!("Error: {}", e),
    }
  }

  Some(fetched_items)
}

use phf::phf_map;

#[derive(Debug, Deserialize, Serialize, Savefile)]
pub struct CachedList {
  pub items: Vec<Item>,
  filename: String,
  // We'd want datetime to be DateTime<Local> but Savefile aint happy with it
  // datetime: DateTime<Local>,
  datetime: String,
}

// #[path = "settings.rs"]
// mod settings;
//
use crate::settings;

fn save_list(settings: settings::Settings, list: &CachedList) {
  let filename = settings::dir_with_filename(&settings.paths.lists_cache_dir, &list.filename);
  let filename = settings::pathbuf_to_str(filename);
  info!("Saving list to path {:?}", filename);
  save_file(&filename, 0, list).unwrap();
}

fn load_list(settings: &settings::Settings, filename: &String) -> CachedList {
  let disk_filename = settings::dir_with_filename(&settings.paths.lists_cache_dir, &filename);
  let disk_filename = settings::pathbuf_to_str(disk_filename);
  load_file(&disk_filename, 0).expect(&format!("Failed to read file {}", disk_filename))
}

fn list_exists(settings: &settings::Settings, filename: &String) -> bool {
  let disk_filename = settings::dir_with_filename(&settings.paths.lists_cache_dir, &filename);
  let disk_filename = settings::pathbuf_to_str(disk_filename);
  return Path::new(&disk_filename).exists();
}

pub static LISTS: phf::Map<&'static str, &'static str> = phf_map! {
    "frontpage" => "topstories",
    "bestpage" => "beststories",
    "newpage" => "newstories",
    "askpage" => "askstories",
    "showpage" => "showstories",
    "jobspage" => "jobstories",
};

use std::{fs, io};

// Retrieves currently stored versions for a list, sorted by date
pub async fn get_list_versions(
  settings: settings::Settings,
  list_name: String,
) -> Option<Vec<CachedList>> {
  let wanted_list = LISTS.get(&list_name).unwrap();
  info!("Found list {:?} to fetch", wanted_list);

  let dir_path = settings.paths.lists_cache_dir.clone();
  let mut entries = fs::read_dir(dir_path)
    .unwrap()
    .map(|res| res.map(|e| e.file_name().into_string().unwrap()))
    .filter(|filename| filename.as_ref().unwrap().contains(wanted_list))
    .collect::<Result<Vec<String>, io::Error>>()
    .unwrap();

  entries.sort();
  entries.reverse();

  let list_versions: Vec<CachedList> = entries
    .clone()
    .into_iter()
    .map(|filename| load_list(&settings, &filename))
    .collect();

  // println!("{:#?}", entries);
  // println!("{:#?}", list_versions);

  // entries = entries
  //     .into_iter()
  //     .filter(|&n| n > from && n < to)
  //     .collect();

  Some(list_versions)
}

// User-facing function to return a list by name + current date + hour
// If the list is available in cache, returns cached list, otherwise fetches
// all the related items and returns a cached list
pub async fn get_list_by_name(
  settings: settings::Settings,
  list_name: String,
) -> Option<CachedList> {
  let wanted_list = LISTS.get(&list_name).unwrap();
  info!("Found list {:?} to fetch", wanted_list);

  let current_time = chrono::offset::Local::now();
  info!("{:?}", current_time);
  let cache_timestamp = current_time.format("%Y-%m-%d-%H").to_string();
  let cache_filename = format!("{}-{}.bin", wanted_list, cache_timestamp);

  info!("Using cache filename {:?}", cache_filename);

  if settings.use_cache && list_exists(&settings, &cache_filename) {
    info!("There was a list already cached on our disk!");
    return Some(load_list(&settings, &cache_filename));
  }

  let items = get_list_page(String::from(format!(
    "https://hacker-news.firebaseio.com/v0/{}.json",
    wanted_list
  )))
  .await
  .unwrap();

  let cached_list = CachedList {
    items: items,
    filename: cache_filename,
    datetime: cache_timestamp,
  };

  if settings.use_cache {
    save_list(settings, &cached_list);
  }

  Some(cached_list)
}

fn rustls_config() -> ClientConfig {
  let mut root_store = RootCertStore::empty();
  root_store.add_server_trust_anchors(webpki_roots::TLS_SERVER_ROOTS.0.iter().map(|ta| {
    OwnedTrustAnchor::from_subject_spki_name_constraints(ta.subject, ta.spki, ta.name_constraints)
  }));

  rustls::ClientConfig::builder()
    .with_safe_defaults()
    .with_root_certificates(root_store)
    .with_no_client_auth()
}

pub async fn get_latest_item_id() -> u32 {
  // let client_tls_config = Arc::new(rustls_config());

  // let client = Client::builder()
  //   // Wikipedia requires a User-Agent header to make requests
  //   .add_default_header((header::USER_AGENT, "rs-hn-threads/1.0"))
  //   // a "connector" wraps the stream into an encrypted connection
  //   .connector(Connector::new().rustls(Arc::clone(&client_tls_config)))
  //   .finish();

  let url_to_get = "https://hacker-news.firebaseio.com/v0/maxitem.json";

  // let mut res = client.get(url_to_get).send().await.unwrap();
  let mut res = isahc::get_async(url_to_get).await.unwrap();

  let latest_id = res.json::<u32>().await.unwrap();
  latest_id
}

use rand::{seq::SliceRandom, thread_rng};

pub async fn get_vec_of_items(skip_cache: bool, items: Vec<u32>) -> Option<Vec<Item>> {
  let mut fetched_items: Vec<Item> = vec![];

  let fetches = futures::stream::iter(
    items
      .into_iter()
      .map(|item_id| tokio::spawn(get_item(skip_cache, item_id))),
  )
  .buffer_unordered(128)
  .map(|r| {
    match r {
      Ok(item) => {
        match item {
          Some(parsed) => fetched_items.push(parsed),
          None => {}
        }
        // fetched_items.push(item.unwrap())
      }
      Err(_) => {}
    }
  })
  .collect::<Vec<_>>();
  fetches.await;

  Some(fetched_items)
}

pub async fn get_items_between(skip_cache: bool, from: u32, to: u32) -> Option<Vec<Item>> {
  let mut vals: Vec<u32> = (from..to).collect();

  vals.shuffle(&mut thread_rng());

  let items: Vec<Item> = get_vec_of_items(skip_cache, vals).await.unwrap();

  Some(items)
}

pub async fn get_latest_items(skip_cache: bool, number_of_items: u32) -> Option<Vec<Item>> {
  let latest_id = get_latest_item_id().await;

  info!("Got the latest ID here: {:?}", latest_id);

  let earliest_id = latest_id - number_of_items;

  return get_items_between(skip_cache, earliest_id, latest_id).await;
}

#[derive(Debug, Deserialize, Serialize)]
struct UserResponse {
  submitted: Vec<u32>,
}

pub async fn get_user_submitted(username: String) -> Option<Vec<u32>> {
  let url = format!(
    "https://hacker-news.firebaseio.com/v0/user/{}.json",
    username,
  );
  let mut res = isahc::get_async(url).await.unwrap();

  let user: UserResponse = res.json::<UserResponse>().await.unwrap();
  Some(user.submitted)
}

pub async fn get_replies_to_user_submitted(username: String) -> Option<Vec<Item>> {
  println!("Getting items user submitted");
  let submitted = get_user_submitted(username).await.unwrap();
  println!("Getting 1000 latest items");
  let latest_items = get_latest_items(false, 100000).await.unwrap();

  let mut replies: Vec<Item> = vec![];

  for item in latest_items {
    match &item.parent {
      Some(parent) => {
        if submitted.contains(parent) {
          replies.push(item)
        }
      }
      None => {}
    }
  }

  Some(replies)
}
