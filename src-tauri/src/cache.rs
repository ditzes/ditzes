use serde::{Deserialize, Serialize};
extern crate savefile;
use dirs::data_dir;
use mkdirp::mkdirp;
use savefile::prelude::*;
use std::{io, path::PathBuf};

#[derive(Debug, Deserialize, Serialize, Savefile)]
pub struct CacheStats {
  earliest_cached_item: u32,
  latest_cached_item: u32,
  latest_item: u32,
}

pub fn get_posts_data_dir() -> String {
  let dir = data_dir().unwrap();
  let app_name = "ditzes";
  let items_directory = "items";
  let p: PathBuf = [dir, PathBuf::from(app_name), PathBuf::from(items_directory)]
    .iter()
    .collect();
  return p.clone().into_os_string().into_string().unwrap();
}

// Duplicate in hn.rs
pub fn install_data_dir() {
  let dir = data_dir().unwrap();
  let app_name = "ditzes";
  let p: PathBuf = [dir, PathBuf::from(app_name)].iter().collect();
  println!(
    "Using {} as data-dir for cache_stats",
    p.clone().into_os_string().into_string().unwrap()
  );
  mkdirp(p).expect("Couldn't create data-dir");
  return;
}

fn construct_path() -> String {
  let dir = data_dir().unwrap();
  let app_name = "ditzes";
  let filename = "cache_stats.bin";
  let p: PathBuf = [dir, PathBuf::from(app_name), PathBuf::from(filename)]
    .iter()
    .collect();

  let path_s: String = p.into_os_string().into_string().unwrap();
  return path_s;
}

pub fn save_cache_stats(cache_stats: &CacheStats) {
  let filename = construct_path();
  save_file(&filename, 0, cache_stats).unwrap();
}

pub fn load_cache_stats() -> CacheStats {
  let filename = construct_path();
  load_file(&filename, 0).unwrap()
}

// fn cache_stats_exists() -> bool {
//   let filename = construct_path();
//   return Path::new(&filename).exists();
// }

use std::fs;

// pub fn update_cache(cache_stats: &CacheStats) {
//   let all_files = fs::read_dir(get_posts_data_dir()).unwrap();
// }

pub fn new() -> CacheStats {
  let cache_stats = CacheStats {
    earliest_cached_item: 0,
    latest_cached_item: 0,
    latest_item: 0,
  };
  cache_stats
}

#[path = "hn.rs"]
mod hn;
use std::time::Instant;

fn imagedata_path() -> String {
  let dir = data_dir().unwrap();
  let app_name = "ditzes";
  let filename = "cache_stats.png";
  let p: PathBuf = [dir, PathBuf::from(app_name), PathBuf::from(filename)]
    .iter()
    .collect();

  let path_s: String = p.into_os_string().into_string().unwrap();
  return path_s;
}

use image::{ImageBuffer, Rgb};
use walkdir::WalkDir;

pub async fn generate_cache_image(from: u32, to: u32) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
  let now = Instant::now();

  let ids: Vec<u32> = (from..to).collect();

  println!("{} possible items in total", ids.len());

  let mut entries = fs::read_dir(get_posts_data_dir())
    .unwrap()
    .map(|res| res.map(|e| e.file_name().into_string().unwrap().parse::<u32>().unwrap()))
    .collect::<Result<Vec<u32>, io::Error>>()
    .unwrap();

  println!("Length: {:?}", entries.len());

  let mut entries2: Vec<String> = Vec::new();
  let post_data_dir = get_posts_data_dir();
  for entry in WalkDir::new(&post_data_dir) {
    let e = entry.expect("Couldn't read filename");
    let absolute_path = e.path();
    let relative_path = absolute_path
      .strip_prefix(&post_data_dir)
      .expect("Couldn't strip base from path");
    let p = relative_path.display().to_string();
    // println!("{}", p);
    if p != "" {
      entries2.push(p);
    }
  }

  let parsed_entries: Vec<u32> = entries2
    .iter()
    .map(|e| {
      let without_slash = e.replace("/", "");
      without_slash
        .parse::<u32>()
        .expect("Couldn't turn filename into u32")
    })
    .collect();

  let mut entries = parsed_entries;

  entries = entries
    .into_iter()
    .filter(|&n| n > from && n < to)
    .collect();

  println!("Length: {:?}", entries.len());
  println!("Length: {:?}", entries2.len());

  println!("Read local cache entries");

  entries.sort();

  println!("Sorted");

  println!("Length: {:?}", entries.len());

  println!("IDs");

  // println!("{:?}", ids);

  let vec_len = ids.len() as usize;
  let mut cache_list = vec![0; vec_len];

  println!("vec_len: {:#?}", vec_len);
  println!("from: {:#?}", from);

  for entry in entries {
    // println!("entry: {:#?}", entry);
    // println!("entry - from: {:#?} - {:#?}", entry, from);
    let idx = (entry - from) as usize;
    // let idx = entry as usize;
    // if idx < cache_list.len() {
    cache_list[idx] = 1;
    // }
  }

  println!("Checked against full list of possible items");
  cache_list.reverse();
  // println!("{:?}", cache_list);
  println!("Generated full cache list");
  // println!("{:?}", cache_list);
  println!("{:?}", cache_list.len());

  let elapsed = now.elapsed();
  println!("Calc Operation took {:.2?} in total", elapsed);

  let now = Instant::now();

  // let path = Path::new(r"./image.png");
  // let file = File::create(path).unwrap();
  // let ref mut w = BufWriter::new(file);

  // This should depend on the width coming from the frontend
  let image_w: u32 = 180;
  let mut image_h: u32 = vec_len as u32 / image_w;

  if image_h == 0 {
    image_h = image_w;
  }

  // let mut pixels: Vec<u8> = vec![0; (image_w as usize * image_h as usize) * 4];
  // println!("{:?}", pixels[0]);
  // println!("{:?}", pixels[4]);

  let mut imgbuf = image::ImageBuffer::new(image_w, image_h);

  // Iterate over the coordinates and pixels of the image
  let mut index = 0 as usize;
  for (_x, _y, pixel) in imgbuf.enumerate_pixels_mut() {
    // let r = (0.3 * x as f32) as u8;
    // let b = (0.3 * y as f32) as u8;
    // let idx: u64 = (x * y) as u64;
    let idx: usize = index;
    index = index + 1;
    // println!("{} {}x {}y", index, x, y);
    if index < cache_list.len() {
      if cache_list[idx] == 1 {
        let r: u8 = 0;
        let g: u8 = 220;
        let b: u8 = 0;
        *pixel = image::Rgb([r, g, b]);
        continue;
      }
      if cache_list[idx] == 0 {
        let r: u8 = 160;
        let g: u8 = 0;
        let b: u8 = 0;
        *pixel = image::Rgb([r, g, b]);
        continue;
      }
    }
    println!("Warning: Writing blue");
    let r: u8 = 0;
    let g: u8 = 0;
    let b: u8 = 150;
    *pixel = image::Rgb([r, g, b]);
  }

  // imgbuf.save(imagedata_path()).unwrap();

  let elapsed = now.elapsed();
  println!("Image Operation took {:.2?} in total", elapsed);
  imgbuf
}

// pub async fn write_imagedata(latest_id: u32) {
//   let now = Instant::now();
//
//   let ids: Vec<u32> = (20227403..latest_id).collect();
//
//   println!("{} possible items in total", ids.len());
//
//   let mut entries = fs::read_dir(get_posts_data_dir())
//     .unwrap()
//     .map(|res| res.map(|e| e.file_name().into_string().unwrap().parse::<u32>().unwrap()))
//     .collect::<Result<Vec<u32>, io::Error>>()
//     .unwrap();
//
//   println!("Read local cache entries");
//
//   entries.sort();
//
//   println!("Sorted");
//
//   let vec_len = latest_id as usize;
//   let mut cache_list = vec![0; vec_len];
//
//   for entry in entries {
//     let idx = entry as usize;
//     cache_list[idx] = 1;
//   }
//
//   println!("Checked against full list of possible items");
//   cache_list.reverse();
//   // println!("{:?}", cache_list);
//   println!("Generated full cache list");
//
//   let elapsed = now.elapsed();
//   println!("Calc Operation took {:.2?} in total", elapsed);
//
//   let now = Instant::now();
//
//   // let path = Path::new(r"./image.png");
//   // let file = File::create(path).unwrap();
//   // let ref mut w = BufWriter::new(file);
//
//   let image_w: u32 = 2000;
//   let image_h: u32 = cache_list.len() as u32 / 2000;
//
//   // let mut pixels: Vec<u8> = vec![0; (image_w as usize * image_h as usize) * 4];
//   // println!("{:?}", pixels[0]);
//   // println!("{:?}", pixels[4]);
//
//   let mut imgbuf = image::ImageBuffer::new(image_w, image_h);
//
//   // Iterate over the coordinates and pixels of the image
//   let mut index = 0;
//   for (_x, _y, pixel) in imgbuf.enumerate_pixels_mut() {
//     // let r = (0.3 * x as f32) as u8;
//     // let b = (0.3 * y as f32) as u8;
//     // let idx: u64 = (x * y) as u64;
//     let idx: usize = index;
//     index = index + 1;
//     // println!("{} {}x {}y", index, x, y);
//     if index < cache_list.len() {
//       if cache_list[idx] == 1 {
//         let r: u8 = 0;
//         let g: u8 = 220;
//         let b: u8 = 0;
//         *pixel = image::Rgb([r, g, b]);
//       }
//       if cache_list[idx] == 0 {
//         let r: u8 = 160;
//         let g: u8 = 0;
//         let b: u8 = 0;
//         *pixel = image::Rgb([r, g, b]);
//       }
//     }
//   }
//
//   imgbuf.save(imagedata_path()).unwrap();
//
//   let elapsed = now.elapsed();
//   println!("Image Operation took {:.2?} in total", elapsed);
// }

use std::io::Read;

pub async fn read_imagedata() -> Vec<u8> {
  let path = imagedata_path();
  //let contents = fs::read_to_string(path).expect("Something went wrong reading the file");

  let f = std::fs::File::open(path).unwrap();
  let mut reader = std::io::BufReader::new(f);
  let mut buffer = Vec::new();

  // Read file into vector.
  reader
    .read_to_end(&mut buffer)
    .expect("Couldnt read image file");

  buffer
}
