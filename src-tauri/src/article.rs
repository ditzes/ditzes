extern crate readability;
use readability::extractor;

// struct Article {
//     submission_id: u32,
// }

pub fn get_article(url: String) -> String {
    match extractor::scrape(&url) {
        Ok(article) => article.content,
        Err(err) => {
            println!("Error fetching article");
            println!("{:?}", err);
            "Error fetching article".to_string()
        }
    }
    // {
    //     Ok(product) => {
    //         println!("------- html ------");
    //         println!("{}", product.content);
    //         println!("---- plain text ---");
    //         println!("{}", product.text);
    //         product.content
    //     },
    //     Err(err) => Err(err)
    // }
}
