use serde::{Deserialize, Serialize};
extern crate savefile;
use dirs::data_dir;
use log::{debug, info, warn};
use mkdirp::mkdirp;
use savefile::prelude::*;
use std::path::{Path, PathBuf};

#[derive(Debug, Deserialize, Serialize, Clone)]
struct UserMute {
    username: String,
    mute_stories: bool,
    mute_comments: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
struct KeywordMute {
    keyword: String,
    mute_stories: bool,
    mute_comments: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
struct UserTag {
    tag: String,
    users: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Paths {
    pub data_dir: PathBuf,
    items_cache_dir: PathBuf,
    pub lists_cache_dir: PathBuf,
    pub search_db_dir: PathBuf,
    readability_cache_dir: PathBuf,
    cache_stats_dir: PathBuf,
    settings_dir: PathBuf,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Settings {
    // The Version of this Settings struct
    pub version: u8,
    // What different paths to use for storing data/cache on disk
    pub paths: Paths,
    // If the client should try to leverage cache for as much as possible
    pub use_cache: bool,
    // If client should try every `auto_cache_interval` to
    // cache `auto_cache_count` amount of items
    pub auto_cache: bool,
    pub auto_cache_interval: u64,
    pub auto_cache_count: u32,
    pub auto_cache_fetch_lists: bool,
    // If we should use darker or lighter color scheme
    dark_mode: bool,
    // If we should use OSes preferred dark/light color scheme
    auto_dark_mode: bool,
    // Users whos replies you'll get a notification for
    notify_for_users: Vec<String>,
    // Users whos stories/comments you want to hide
    muted_users: Vec<UserMute>,
    // Keywords to use for hiding stories/comments
    muted_keywords: Vec<KeywordMute>,
    // Custom tags to apply for users
    user_tags: Vec<UserTag>,
    // If API/frontend should be allowed to mutate state or not
    pub read_only_mode: bool,
    // Show debug output or not
    debug: bool,
}

// Duplicate in hn.rs
pub fn install_data_dir() {
    let dir = data_dir().unwrap();
    let app_name = "ditzes";
    let p: PathBuf = [dir, PathBuf::from(app_name)].iter().collect();
    info!(
        "Using {} as data-dir for settings",
        p.clone().into_os_string().into_string().unwrap()
    );
    mkdirp(p).expect("Couldn't create data-dir");
    return;
}

fn construct_path() -> String {
    let dir = data_dir().unwrap();
    let app_name = "ditzes";
    let filename = "settings.json";
    let p: PathBuf = [dir, PathBuf::from(app_name), PathBuf::from(filename)]
        .iter()
        .collect();

    let path_s: String = p.into_os_string().into_string().unwrap();
    return path_s;
}

use serde_json;
use std::fs;

pub fn save_settings(settings: &Settings) {
    let filename = construct_path();
    let json_string = serde_json::to_string_pretty(&settings).expect("Could not encode JSON value");
    info!("{}", json_string);
    fs::write(filename, json_string).expect("Could not write to file!");
}

pub fn load_settings() -> Settings {
    let filename = construct_path();
    let file_str = fs::read_to_string(filename).expect("Could not read Settings file");
    let settings: Settings =
        serde_json::from_str(&file_str).expect("Could not parse Settings JSON");
    info!("{:#?}", settings);
    settings
}

fn settings_exists() -> bool {
    let filename = construct_path();
    return Path::new(&filename).exists();
}

// Return datadir for user, creating it if needed
pub fn get_data_dir() -> PathBuf {
    let dir = data_dir().unwrap();
    let app_name = "ditzes";
    let p: PathBuf = [dir, PathBuf::from(app_name)].iter().collect();
    let p_str = p.clone().into_os_string().into_string().unwrap();
    info!("Using {} as data-dir for items", p_str);
    mkdirp(p.clone()).expect("Couldn't create data-dir");
    p
}

// Returns full path for root_dir + append_dir, creating it if needed
pub fn append_to_dir(root_dir: &PathBuf, to_append: &str) -> PathBuf {
    let p: PathBuf = [root_dir, &PathBuf::from(to_append)].iter().collect();
    let p_str = p.clone().into_os_string().into_string().unwrap();
    info!("Using {} as data-dir for {}", p_str, to_append);
    mkdirp(&p).expect("Couldn't create data-dir");
    p
}

pub fn dir_with_filename(root_dir: &PathBuf, to_append: &str) -> PathBuf {
    let p: PathBuf = [root_dir, &PathBuf::from(to_append)].iter().collect();
    let p_str = p.clone().into_os_string().into_string().unwrap();
    info!("Using {} as data-dir for {}", p_str, to_append);
    p
}

pub fn pathbuf_to_str(pb: PathBuf) -> String {
    return pb.clone().into_os_string().into_string().unwrap();
}

// Either loads existing settings or create a new default settings and persists it
pub fn load_or_setup_settings() -> Settings {
    if settings_exists() {
        return load_settings();
    }
    let data_dir = get_data_dir();
    let paths = Paths {
        data_dir: data_dir.clone(),
        items_cache_dir: append_to_dir(&data_dir, "items"),
        lists_cache_dir: append_to_dir(&data_dir, "lists"),
        search_db_dir: append_to_dir(&data_dir, "search"),
        readability_cache_dir: append_to_dir(&data_dir, "readability"),
        cache_stats_dir: append_to_dir(&data_dir, "cache"),
        settings_dir: append_to_dir(&data_dir, "settings"),
    };
    let settings = Settings {
        version: 0, // We'll use version 0 until out of Alpha
        paths,
        use_cache: true,
        auto_cache: false,
        auto_cache_interval: 60,
        auto_cache_count: 1000,
        auto_cache_fetch_lists: true,
        dark_mode: false,
        auto_dark_mode: true,
        muted_keywords: Vec::new(),
        muted_users: Vec::new(),
        notify_for_users: Vec::new(),
        user_tags: Vec::new(),
        read_only_mode: false,
        debug: false,
    };
    save_settings(&settings);
    return settings;
}
