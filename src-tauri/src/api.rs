use actix_cors::Cors;
use actix_web::{
  get, middleware::Logger, post, put, web, web::Data, App, HttpRequest, HttpResponse, Responder,
  Result,
};

use crate::{
  build_info, cache, hn, logs, search, settings, site_icon, update_service::UpdateService,
};

// use notify_rust::Notification;
use actix_web::error;
use image::{DynamicImage, ImageOutputFormat};
use log::{info, warn};
use serde::Deserialize;
use std::io::Cursor;

fn forbidden_err() -> actix_web::Error {
  warn!("Attempted access to endpoint denied because of read-only-mode");
  error::ErrorUnauthorized(
    "{\"is_error\": true, \"message\": \"read-only-mode active, endpoint not accessible\"}",
  )
}

// #[get("/api/frontpage")]
// async fn frontpage() -> Result<impl Responder> {
//   let items = hn::get_frontpage().await.unwrap();
//   Ok(web::Json(items))
// }

//async fn get_list(req: HttpRequest, list_name: String) -> Result<web::Json<Vec<hn::Item>>> {
async fn get_list(req: HttpRequest, list_name: String) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.get_ref();
  let list = hn::get_list_by_name(settings.clone(), list_name)
    .await
    .unwrap();
  Ok(web::Json(list))
}

#[get("/api/list/{page}")]
async fn http_get_list(page: web::Path<String>, req: HttpRequest) -> Result<impl Responder> {
  get_list(req, page.to_string()).await
}

#[get("/api/list/{page}/versions")]
async fn http_get_list_versions(
  page: web::Path<String>,
  req: HttpRequest,
) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.get_ref();
  let list_name = page.to_string();
  let list = hn::get_list_versions(settings.clone(), list_name)
    .await
    .unwrap();
  Ok(web::Json(list))
}

#[get("/api/comments/{id}")]
async fn comments(id: web::Path<u32>) -> Result<impl Responder> {
  let id: u32 = id.to_string().parse().unwrap();
  let items = hn::get_items(id).await.unwrap();
  Ok(web::Json(items))
}

#[get("/api/update/{n}")]
async fn update(req: HttpRequest, n: web::Path<u32>) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.as_ref();
  if settings.read_only_mode {
    return Err(forbidden_err());
  }

  let n: u32 = n.to_string().parse().unwrap();

  use std::time::Instant;
  let now = Instant::now();

  let items = hn::get_latest_items(false, n).await.unwrap();

  let elapsed = now.elapsed();
  let res = format!("Fetched {} items in {:.2?}", items.len(), elapsed);

  Ok(web::Json(res))
}

#[get("/api/latest_item_id")]
async fn latest_item_id() -> Result<impl Responder> {
  use std::time::Instant;
  let now = Instant::now();

  let res = hn::get_latest_item_id().await;

  let elapsed = now.elapsed();
  format!("Fetched latest item id {:.2?}", elapsed);

  Ok(web::Json(res))
}

#[post("/api/update/{start_id}/{end_id}")]
async fn update_between(req: HttpRequest, param: web::Path<(u32, u32)>) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.as_ref();
  if settings.read_only_mode {
    return Err(forbidden_err());
  }

  let (start_id, end_id) = param.into_inner();
  if start_id != 0 && end_id != 0 {
    let items = hn::get_items_between(false, start_id, end_id).await;
    Ok(web::Json(items))
  } else {
    Ok(web::Json(None))
  }
}

#[get("/api/cache/image_data/{start_id}/{end_id}/image.png")]
async fn cache_image_data(param: web::Path<(u32, u32)>) -> HttpResponse {
  // TODO if we're in read-only mode, we want to hardcode the start/end IDs
  // to prevent abuse
  //
  // let start_id: u32 = start_id.to_string().parse().unwrap();
  // let end_id: u32 = start_id.to_string().parse().unwrap();
  let (start_id, end_id) = param.into_inner();

  let imagedata = cache::generate_cache_image(start_id, end_id).await;

  let mut w = Cursor::new(Vec::new());
  DynamicImage::ImageRgb8(imagedata)
    .write_to(&mut w, ImageOutputFormat::Png)
    .unwrap();

  // Ok(Bytes::from(imagedata))
  return HttpResponse::Ok()
    .content_type("image/png")
    .body(w.into_inner());
}

#[get("/api/cache/image_data.png")]
async fn get_cached_image_data() -> HttpResponse {
  let imagedata: Vec<u8> = cache::read_imagedata().await;

  let bytes = actix_web::web::Bytes::from(imagedata);

  HttpResponse::Ok().content_type("image/png").body(bytes)
}

#[derive(Deserialize)]
struct PendingNotification {
  text: String,
}

// #[post("/api/notify")]
// async fn notify(
//   req: HttpRequest,
//   pending_notification: web::Json<PendingNotification>,
// ) -> Result<String> {
//   let settings = req.app_data::<Data<settings::Settings>>().unwrap();
//   let settings = settings.as_ref();
//   if settings.read_only_mode {
//     return Err(forbidden_err());
//   }
//
//   println!("NOTIFY! {:?}", pending_notification.text);
//   Notification::new()
//     .summary("Ditzes")
//     .body(&pending_notification.text)
//     .show()
//     .expect("Couldn't show notification");
//   Ok("true".to_string())
// }

#[get("/api/settings")]
async fn get_settings() -> Result<impl Responder> {
  let mut settings = settings::load_settings();
  match env::var("DITZES_READ_ONLY") {
    Ok(_) => {
      println!("Forcing read only mode");
      settings.read_only_mode = true;
    }
    _ => {}
  }
  Ok(web::Json(settings))
}

#[put("/api/settings")]
async fn put_settings(
  req: HttpRequest,
  new_settings: web::Json<settings::Settings>,
) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.as_ref();
  if settings.read_only_mode {
    return Err(forbidden_err());
  }

  let us = req.app_data::<Data<UpdateService>>().unwrap();
  let us = us.as_ref();
  // Check if settings regarding the UpdateService is different
  if new_settings.auto_cache {
    us.start().await;
  } else {
    us.stop().await;
  }

  settings::save_settings(&new_settings);
  Ok(web::Json(new_settings))
}

#[get("/api/build-info")]
async fn get_build_info() -> Result<impl Responder> {
  let build_info = build_info::new();
  Ok(web::Json(build_info))
}

#[get("/api/search/query/{term}")]
async fn search_query(search_term: web::Path<String>, req: HttpRequest) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.get_ref();

  let term = search_term.to_string();

  let results = search::query_index(settings, term).await.unwrap();

  Ok(web::Json(results))
}

#[post("/api/search/index")]
async fn search_index(req: HttpRequest) -> Result<impl Responder> {
  let settings = req.app_data::<Data<settings::Settings>>().unwrap();
  let settings = settings.as_ref();
  if settings.read_only_mode {
    return Err(forbidden_err());
  }

  let results = search::create_index(settings).await.unwrap();
  Ok(web::Json(results))
}

#[get("/api/favicon/{domain}")]
async fn external_favicon(domain: web::Path<String>, req: HttpRequest) -> Result<impl Responder> {
  let domain = domain.to_string();

  // let url = match get_favicon_url(domain).await {
  //     Some(u) => u,
  //     None => {
  //         let conn_info = req.connection_info();
  //         let host = conn_info.host();
  //         let schema = conn_info.scheme();
  //         format!("{}://{}/site-default-favicon.png", schema, host)
  //     }
  // };

  // let client = HttpClient::builder()
  //     .timeout(Duration::from_secs(5))
  //     .default_header("user-agent", "Mozilla/5.0 (X11; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0")
  //     .redirect_policy(RedirectPolicy::Limit(10))
  //     .build()
  //     .unwrap();

  // let mut response = client.get_async(url).await.unwrap();

  //let bytes = response.bytes().await.unwrap();

  let site_icon = site_icon::get_icon_bytes(domain, req).await;

  Ok(
    HttpResponse::Ok()
      .content_type(site_icon.content_type)
      .append_header(("Cache-Control", "max-age=86400"))
      .body(site_icon.bytes.to_vec()),
  )
}

use http::StatusCode;

#[get("/")]
async fn index(req: HttpRequest) -> Result<impl Responder> {
  let conn_info = req.connection_info().clone();
  let should_redirect = match env::var("DITZES_REDIRECT_TO_HTTPS") {
    Ok(_) => true,
    _ => false,
  };
  if should_redirect && conn_info.scheme() == "http" {
    let host = conn_info.host();
    let path = req.uri();
    let new_location = format!("https://{}{}", host, path);
    println!("Redirect to {:?} {:?}", host, path);
    return Ok(
      HttpResponse::Ok()
        .status(StatusCode::MOVED_PERMANENTLY)
        .append_header(("Location", new_location))
        .body(""),
    );
  }

  let bytes = include_bytes!("../../dist/index.html");
  Ok(
    HttpResponse::Ok()
      .content_type("text/html")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(String::from_utf8(bytes.to_vec()).unwrap()),
  )
}

#[get("/{tail}*")]
async fn notfound(req: HttpRequest) -> Result<impl Responder> {
  let conn_info = req.connection_info().clone();
  let should_redirect = match env::var("DITZES_REDIRECT_TO_HTTPS") {
    Ok(_) => true,
    _ => false,
  };
  if should_redirect && conn_info.scheme() == "http" {
    let host = conn_info.host();
    let path = req.uri();
    let new_location = format!("https://{}{}", host, path);
    println!("Redirect to {:?} {:?}", host, path);
    return Ok(
      HttpResponse::Ok()
        .status(StatusCode::MOVED_PERMANENTLY)
        .append_header(("Location", new_location))
        .body(""),
    );
  }

  let bytes = include_bytes!("../../dist/index.html");
  Ok(
    HttpResponse::Ok()
      .content_type("text/html")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(String::from_utf8(bytes.to_vec()).unwrap()),
  )
}

#[get("/js/main.js")]
async fn js_main(_req: HttpRequest) -> Result<impl Responder> {
  let bytes = include_bytes!("../../dist/js/main.js");
  Ok(
    HttpResponse::Ok()
      .content_type("application/javascript")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(String::from_utf8(bytes.to_vec()).unwrap()),
  )
}

#[get("/css/style.css")]
async fn css_style(_req: HttpRequest) -> Result<impl Responder> {
  let bytes = include_bytes!("../../dist/css/style.css");
  Ok(
    HttpResponse::Ok()
      .content_type("text/css")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(String::from_utf8(bytes.to_vec()).unwrap()),
  )
}

#[get("/robots.txt")]
async fn robots(_req: HttpRequest) -> Result<impl Responder> {
  let bytes = include_bytes!("../../dist/robots.txt");
  Ok(
    HttpResponse::Ok()
      .content_type("plain/text")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(String::from_utf8(bytes.to_vec()).unwrap()),
  )
}

#[get("/favicon.ico")]
async fn favicon(_req: HttpRequest) -> Result<impl Responder> {
  let bytes = include_bytes!("../../dist/favicon.ico");
  Ok(
    HttpResponse::Ok()
      .content_type("image/x-icon")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(bytes.to_vec()),
  )
}

#[get("/site-default-favicon.png")]
async fn site_default_favicon(_req: HttpRequest) -> Result<impl Responder> {
  let bytes = include_bytes!("../../dist/default-favicon.png");
  Ok(
    HttpResponse::Ok()
      .content_type("image/png")
      .append_header(("Cache-Control", "max-age=86400"))
      .body(bytes.to_vec()),
  )
}

use actix_web::{dev::ServiceRequest, HttpServer};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
use std::{
  env,
  net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4},
};

// Removes the two last digits from IP
fn mask_ip(req: &ServiceRequest) -> String {
  match req.peer_addr().unwrap() {
    SocketAddr::V4(res) => {
      let mut octets = res.ip().octets();
      octets[octets.len() - 2] = 255;
      octets[octets.len() - 1] = 255;
      let new = Ipv4Addr::new(octets[0], octets[1], octets[2], octets[3]);
      new.to_string()
    }
    SocketAddr::V6(_res) => {
      // TODO we don't actually support ipv6, so return ipv4 address
      // of localhost for now
      "127.0.0.1".to_string()
      // let mut octets = res.ip().octets();
      // octets[octets.len() - 3] = 1;
      // octets[octets.len() - 2] = 1;
      // octets[octets.len() - 1] = 1;
      // let new = Ipv6Addr::new(
      //     octets[0] as u16,
      //     octets[1] as u16,
      //     octets[2] as u16,
      //     octets[3] as u16,
      //     octets[4] as u16,
      //     octets[5] as u16,
      //     octets[6] as u16,
      //     octets[7] as u16
      //     );
      // new.to_string()
    }
  }
}

use actix_web::middleware;

// #[actix_web::main]
pub async fn async_main() {
  logs::init();
  hn::install_data_dir();
  site_icon::install_data_dir();

  let mut _settings = settings::load_or_setup_settings();

  match env::var("DITZES_READ_ONLY") {
    Ok(_) => {
      println!("Forcing read only mode");
      _settings.read_only_mode = true;
    }
    _ => {}
  }

  let _update_service = UpdateService::new_from_settings(&_settings).await;

  let update_service = Data::new(_update_service);
  let settings = Data::new(_settings);

  info!("Starting UpdateService");
  update_service.start().await;

  let http_address = match env::var("DITZES_HTTP_ADDRESS") {
    Ok(address) => address,
    _ => "127.0.0.1".to_string(),
  };

  let http_port = match env::var("DITZES_HTTP_PORT") {
    Ok(port) => port,
    _ => "8080".to_string(),
  };

  let bind_address = format!("{}:{}", http_address, http_port);
  println!("Server gonna listen at http://{:?}", bind_address);

  let allowed_origin_self = format!("http://{}", &bind_address);

  // SSL certs setup
  let https_key = match env::var("DITZES_HTTPS_KEY") {
    Ok(key) => key,
    _ => "../dev-certs/nopass.pem".to_string(),
  };

  let https_cert = match env::var("DITZES_HTTPS_CERT") {
    Ok(cert) => cert,
    _ => "../dev-certs/cert.pem".to_string(),
  };

  let https_address = match env::var("DITZES_HTTPS_ADDRESS") {
    Ok(address) => address,
    _ => "127.0.0.1".to_string(),
  };

  let https_port = match env::var("DITZES_HTTPS_PORT") {
    Ok(port) => port,
    _ => "8081".to_string(),
  };

  let https_bind_address = format!("{}:{}", https_address, https_port);
  println!("Server gonna listen at https://{:?}", https_bind_address);

  let mut builder = SslAcceptor::mozilla_modern(SslMethod::tls()).unwrap();
  builder
    .set_private_key_file(https_key, SslFiletype::PEM)
    .unwrap();
  builder.set_certificate_chain_file(https_cert).unwrap();

  HttpServer::new(move || {
    // Common Log Format (CLF)
    // %h %^[%d:%t %^] "%r" %s %b "%R" "%u"
    // 127.0.0.1 user-identifier frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 google.com "httppie/1.1.1"
    // log with real IP
    // let log_format = "%a - - [%t] \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\"";
    // log with masked IP (127.0.255.255) two last digits will always be 255.255
    let log_format = "%{mip}xi - - [%t] \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\"";

    let logger = Logger::new(log_format).custom_request_replace("mip", |req| mask_ip(req));

    let cors = Cors::default()
      .allowed_origin("http://localhost:8000") // development web ui
      .allowed_origin("http://localhost:8080") // development web ui
      .allowed_origin("tauri://localhost") // Tauri UI
      .allowed_origin(&allowed_origin_self)
      .allowed_methods(vec!["GET", "PUT", "POST"])
      .allowed_headers(vec![http::header::CONTENT_TYPE, http::header::ACCEPT])
      .max_age(3600);
    App::new()
      .app_data(Data::clone(&settings))
      .app_data(Data::clone(&update_service))
      .wrap(logger)
      // .wrap(Logger::new("%a \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\" %T"))
      .wrap(cors)
      .wrap(middleware::Compress::default())
      .service(index) // WebUI HTML
      .service(js_main) // WebUI JS
      .service(css_style) // WebUI CSS
      .service(robots) // WebUI robots.txt
      .service(favicon) // WebUI favicon.ico
      .service(site_default_favicon) // for when we cant find sites favicon
      .service(search_query)
      .service(search_index)
      .service(external_favicon)
      .service(latest_item_id)
      .service(update)
      .service(comments)
      // .service(notify)
      .service(get_settings)
      .service(put_settings)
      .service(get_build_info)
      .service(cache_image_data)
      .service(get_cached_image_data)
      .service(update_between)
      .service(http_get_list)
      .service(http_get_list_versions)
      .service(notfound) // WebUI push state 404 handler
  })
  // .service(comments))
  .disable_signals()
  .workers(12)
  .bind(bind_address)
  .expect(&format!("Couldnt bind to http port {}", http_port))
  .bind_openssl(https_bind_address, builder)
  .expect(&format!("Couldnt bind to https port {}", https_port))
  .run()
  .await
  .unwrap()
}

// async fn frontpage() -> Result<impl Reply, Infallible> {
//     let items = hn::get_frontpage().await.unwrap();
//     let items_json = json!(items);
//     Ok(warp::reply::with_status(items_json.to_string(), StatusCode::OK))
// }
//
// #[tokio::main]
// async fn main() {
//     hn::install_data_dir();
//
//     let hello = warp::path!("hello" / String)
//         .map(|name| format!("Hello, {}!", name));
//
//     // let frontpage = warp::path!("frontpage")
//     //     .and_then(|| async move {
//     //         frontpage()
//     //     });
//     let frontpage = warp::path!("frontpage")
//         .and_then(|| {
//             Ok(frontpage())
//         });
//
//     let routes = warp::get().and(
//         hello
//         .or(frontpage)
//         );
//
//     warp::serve(routes)
//         .run(([127, 0, 0, 1], 3030))
//         .await;
// }
