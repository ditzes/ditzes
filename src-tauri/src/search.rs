use futures::future;
use std::{fs, io};
use tantivy::{collector::TopDocs, query::QueryParser, schema::*, Index, ReloadPolicy};
// use tempfile::TempDir;
use log::{debug, info, warn};

#[path = "cache.rs"]
mod cache;

#[path = "hn.rs"]
mod hn;

// #[path = "settings.rs"]
// mod settings;
use crate::settings;

use std::path::PathBuf;
use tantivy::schema::Schema;

fn get_tantivy_index(
  path: &PathBuf,
  schema: Schema,
) -> Result<tantivy::Index, Box<dyn std::error::Error>> {
  let path_str = path.clone().into_os_string().into_string().unwrap();
  println!("Using path {:?} for search index", path_str);
  let index = Index::create_in_dir(path, schema.clone()).or_else(|error| match error {
    tantivy::TantivyError::IndexAlreadyExists => Ok(Index::open_in_dir(path)?),
    _ => Err(error),
  })?;
  Ok(index)
}

fn get_tantivy_schema() -> Schema {
  let mut schema_builder = Schema::builder();
  schema_builder.add_u64_field("id", FAST | STORED);
  schema_builder.add_text_field("title", TEXT | STORED);
  schema_builder.add_text_field("url", TEXT | STORED);
  schema_builder.add_text_field("domain", STRING | STORED);
  schema_builder.add_text_field("by", STRING | STORED);
  schema_builder.add_text_field("text", TEXT | STORED);
  schema_builder.add_text_field("type", STRING | STORED);
  let schema = schema_builder.build();
  return schema;
}

pub async fn create_index(settings: &settings::Settings) -> Result<u64, io::Error> {
  // let index_path = MmapDirectory::open(settings.paths.search_db_dir)r
  let index_path = &settings.paths.search_db_dir;

  let schema = get_tantivy_schema();

  // let index = Index::open_or_create(&index_path, schema.clone()).unwrap();
  // let index = Index::open_in_dir(&index_path).unwrap();
  // let index = Index::create_in_dir(&index_path, schema.clone()).unwrap();
  let index = get_tantivy_index(&index_path, schema.clone()).unwrap();

  let mut index_writer = index.writer(50_000_000).unwrap();

  println!("Clearing any existing Documents from Index before building new index");

  index_writer.delete_all_documents().unwrap();

  println!("Committing clearing...");

  index_writer.commit().unwrap();

  let id_field = schema.get_field("id").unwrap();
  let title_field = schema.get_field("title").unwrap();
  let url_field = schema.get_field("url").unwrap();
  let domain_field = schema.get_field("domain").unwrap();
  let by_field = schema.get_field("by").unwrap();
  let text_field = schema.get_field("text").unwrap();
  let type_field = schema.get_field("type").unwrap();

  println!("Schema built, reading cached items");

  let item_ids = fs::read_dir(cache::get_posts_data_dir())
    .unwrap()
    .map(|res| res.map(|e| e.file_name().into_string().unwrap().parse::<u32>().unwrap()))
    .collect::<Result<Vec<u32>, io::Error>>()
    .unwrap();

  let tasks: Vec<_> = item_ids
    .iter()
    // .take(25)
    .map(|id| {
      let item_id = id.clone();
      tokio::spawn(async move {
        let item = hn::get_item(false, item_id).await.unwrap();
        return item;
      })
    })
    .collect();

  let items = future::join_all(tasks).await;

  let mut fetched_items: Vec<hn::Item> = vec![];

  for item in items {
    match item {
      Ok(i) => fetched_items.push(i),
      Err(e) => warn!("Error: {}", e),
    }
  }

  println!(
    "{} items found on disk, {} items read",
    item_ids.len(),
    fetched_items.len()
  );

  // let mut docs: Vec<Document> = vec![];

  println!("Adding cached items as Documents");

  for item in fetched_items {
    let mut new_doc = Document::default();
    // println!("### ITEM ###");
    // println!("{:#?}", item.clone());
    let deleted = match item.deleted {
      Some(val) => val,
      None => false,
    };
    if deleted {
      continue;
    }
    new_doc.add_u64(id_field, item.id as u64);
    new_doc.add_text(type_field, item.r#type);

    match item.title {
      Some(title) => new_doc.add_text(title_field, title),
      None => {}
    }

    match item.url {
      Some(url) => new_doc.add_text(url_field, url),
      None => {}
    }

    match item.domain {
      Some(domain) => new_doc.add_text(domain_field, domain),
      None => {}
    }

    match item.text {
      Some(text) => new_doc.add_text(text_field, text),
      None => {}
    }

    match item.by {
      Some(by) => new_doc.add_text(by_field, by),
      None => {}
    }
    // println!("### DOCUMENT ###");
    // println!("{:#?}", new_doc);
    index_writer.add_document(new_doc);
    // println!("Added item doc");
  }

  println!("Committing added Documents");

  let res = index_writer.commit().unwrap();

  println!("Committed documents. DocStamp => {:?}", res);
  Ok(res)
}

use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct SearchResult {
  pub id: u32,
  pub score: f32,
  pub title: Option<String>,
  pub url: Option<String>,
  pub domain: Option<String>,
  pub by: Option<String>,
  pub text: Option<String>,
  pub r#type: String,
}

use std::collections::HashMap;

fn get_text_from_field(doc: &tantivy::Document, field: tantivy::schema::Field) -> Option<String> {
  let val = doc.get_first(field)?.text()?;
  Some(val.to_string())
}

fn get_u32_from_field(doc: &tantivy::Document, field: tantivy::schema::Field) -> Option<u32> {
  let val = doc.get_first(field)?.u64_value()?;
  Some(val as u32)
}

pub async fn query_index(
  settings: &settings::Settings,
  query: String,
) -> Option<Vec<SearchResult>> {
  let index_path = &settings.paths.search_db_dir;

  let schema = get_tantivy_schema();
  let index = get_tantivy_index(index_path, schema.clone()).unwrap();

  let reader = index
    .reader_builder()
    .reload_policy(ReloadPolicy::OnCommit)
    .try_into()
    .unwrap();

  let searcher = reader.searcher();

  let id_field = schema.get_field("id").unwrap();
  let title_field = schema.get_field("title").unwrap();
  let url_field = schema.get_field("url").unwrap();
  let domain_field = schema.get_field("domain").unwrap();
  let by_field = schema.get_field("by").unwrap();
  let text_field = schema.get_field("text").unwrap();
  let type_field = schema.get_field("type").unwrap();

  let query_parser = QueryParser::for_index(
    &index,
    vec![
      title_field,
      url_field,
      domain_field,
      by_field,
      text_field,
      type_field,
    ],
  );

  let query = query_parser.parse_query(&query).unwrap();
  let top_docs = searcher.search(&query, &TopDocs::with_limit(10)).unwrap();

  let mut docs_to_return = vec![];

  for (score, doc_address) in top_docs {
    let retrieved_doc = searcher.doc(doc_address).unwrap();
    // let doc = retrieved_doc.field_values().iter().fold(HashMap::new(), |mut acc, field_value| {
    //     acc.insert(field_value.field(), field_value.value());
    //     acc
    // });
    println!("{:#?}", retrieved_doc);

    let search_result = SearchResult {
      id: get_u32_from_field(&retrieved_doc, id_field).unwrap(),
      score: score,
      title: get_text_from_field(&retrieved_doc, title_field),
      url: get_text_from_field(&retrieved_doc, url_field),
      domain: get_text_from_field(&retrieved_doc, domain_field),
      by: get_text_from_field(&retrieved_doc, by_field),
      text: get_text_from_field(&retrieved_doc, text_field),
      r#type: get_text_from_field(&retrieved_doc, type_field).unwrap(),
    };
    // let title = match retrieved_doc.get_first(title_field) {
    //   Some(field) => match field.text() {
    //     Some(title) => title,
    //     None => "",
    //   },
    //   None => "",
    // };
    // println!("{:#?}", title);
    // println!("{:#?}", search_result);
    // let search_result = SearchResult{};
    // let search_result: SearchResult = retrieved_doc.clone().into();
    docs_to_return.push(search_result);
    // println!("{}", schema.to_json(&retrieved_doc));
    // println!("{:#?}", retrieved_doc);
  }
  Some(docs_to_return)
}
