use vergen::{vergen, Config};

fn main() {
    vergen(Config::default()).unwrap();
    tauri_build::build()
}
