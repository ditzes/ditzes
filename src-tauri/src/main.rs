#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use ditzes::api;
use tauri::Manager;

// use serde_json::json;
// use tauri::{App, AppHandle, Event, Manager};

// for global events, it also must implement `Clone`.
// #[derive(Clone, serde::Serialize, serde::Deserialize)]
// struct Payload {
//   items: String,
// }
//
// #[tauri::command]
// async fn get_comments(id: u32) -> Option<Vec<Item>> {
//   // Call another async function and wait for it to finish
//   // let result = some_async_function().await;
//   println!("Result: {}", id);
//   let items = hn::get_items(id).await.unwrap();
//
//   Some(items)
// }
//
// #[tauri::command]
// async fn get_frontpage() -> Option<Vec<Item>> {
//   let settings = settings::load_settings();
//   let cached_list = hn::get_list_by_name(settings, "frontpage".to_string()).await.unwrap();
//   Some(cached_list.items)
// }
//
// #[tauri::command]
// async fn get_settings() -> Settings {
//   let settings = settings::load_settings();
//   settings
// }

use std::thread;

// Should be able to do something like this to catch back/forward button and
// forward it to the frontend that can handle the navigation itself
// use rdev::{listen, Event, EventType, Button};

// fn callback(event: Event) {
//     println!("My callback {:?}", event);
//     match event.event_type {
//         EventType::ButtonPress(Button::Unknown(8)) => {
//             println!("Pressed back button!")
//         }
//         EventType::ButtonPress(Button::Unknown(9)) => {
//             println!("Pressed forward button!")
//         }
//         _ => {}
//     }
// }

fn main() {
    // hn::install_data_dir();
    // settings::load_or_setup_settings();



    thread::spawn(move || {
        actix_web::rt::System::with_tokio_rt(|| {
            tokio::runtime::Builder::new_multi_thread()
                .enable_all()
                .worker_threads(8)
                .thread_name("main-tokio")
                .build()
                .unwrap()
        })
        .block_on(api::async_main());
    });

    tauri::Builder::default()
        // .setup(|app| {
        //     println!("Within app");
        //     let id = app.get_window("");
        //     app.handle().send_tao_window_event(id, "hello");
        //     Ok(())
            // if let Err(error) = listen(callback) {
            //     println!("Error: {:?}", error)
            // }


        //   // listen to the `event-name` (emitted on any window)
        //   let app_handle: AppHandle = app.handle();
        //   app.listen_global("fetch-item", move |event| {
        //     println!("got event-name with payload {:?}", event.payload());
        //     let id = event.payload().unwrap().parse::<u32>().unwrap();
        //     let items = hn::get_items(id);
        //     let items_json = json!(items);
        //     app_handle.emit_all("fetched-items", Payload { items: items_json.to_string() }).unwrap();
        //   });
        //   // unlisten to the event using the `id` returned on the `listen_global` function
        //   // an `once_global` API is also exposed on the `App` struct
        //   // app.unlisten(id);
        //   // emit the `event-name` event to all webview windows on the frontend
        //   Ok(())
        // })
        //.invoke_handler(tauri::generate_handler![
        //  get_comments,
        //  get_frontpage,
        //  get_settings
        //])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
