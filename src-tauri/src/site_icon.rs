use serde::{Deserialize, Serialize};
extern crate savefile;
use dirs::data_dir;
use log::{debug, info, warn};
use mkdirp::mkdirp;
use savefile::prelude::*;
use std::path::{Path, PathBuf};

#[derive(Debug, Deserialize, Serialize, Savefile, Clone)]
pub struct SiteIcon {
    pub domain: String,
    pub content_type: String,
    pub bytes: Vec<u8>,
}

// Duplicate in settings.rs
pub fn install_data_dir() {
    let dir = data_dir().unwrap();
    let app_name = "ditzes";
    let items_directory = "site_icons";
    let p: PathBuf = [dir, PathBuf::from(app_name), PathBuf::from(items_directory)]
        .iter()
        .collect();
    info!(
        "Using {} as data-dir for items",
        p.clone().into_os_string().into_string().unwrap()
    );
    mkdirp(p).expect("Couldn't create data-dir");
    return;
}

pub fn construct_path(domain: &str) -> String {
    let dir = data_dir().unwrap();
    let filename = domain.to_string();
    let app_name = "ditzes";
    let items_directory = "site_icons";
    let p: PathBuf = [
        dir,
        PathBuf::from(app_name),
        PathBuf::from(items_directory),
        PathBuf::from(filename),
    ]
    .iter()
    .collect();

    let path_s: String = p.into_os_string().into_string().unwrap();
    // info!("Using data dir {}", path_s);
    return path_s;
}

fn save_item(item: &SiteIcon) {
    let filename = construct_path(&item.domain);
    save_file(&filename, 0, item).unwrap();
}

fn load_item(domain: String) -> SiteIcon {
    let filename = construct_path(&domain);
    load_file(&filename, 0).expect(&format!("Failed to read file {}", domain))
}

fn item_exists(domain: String) -> bool {
    let filename = construct_path(&domain);
    return Path::new(&filename).exists();
}

use actix_web::HttpRequest;
use isahc::{config::RedirectPolicy, prelude::*, HttpClient};
use siteicon::get_favicon_url;
use std::{io::Bytes, time::Duration};

pub async fn get_icon_bytes(domain: String, req: HttpRequest) -> SiteIcon {
    if item_exists(domain.clone()) {
        let item = load_item(domain);
        return item;
    }

    let conn_info = req.connection_info();
    let host = conn_info.host();
    let schema = conn_info.scheme();
    let default_favicon = format!("{}://{}/site-default-favicon.png", schema, host);

    let url = match get_favicon_url(domain.clone()).await {
        Some(u) => {
            if u.contains("data:image/svg+xml") {
                println!("Using default favicon");
                default_favicon
            } else {
                u
            }
        }
        None => {
            println!("Using default favicon");
            default_favicon
        }
    };

    let client = HttpClient::builder()
        .timeout(Duration::from_secs(5))
        .default_header(
            "user-agent",
            "Mozilla/5.0 (X11; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0",
        )
        .redirect_policy(RedirectPolicy::Limit(10))
        .build()
        .unwrap();

    println!("{:#?}", url);
    let mut response = client.get_async(url).await.unwrap();

    let content_type = response
        .headers()
        .get("content-type")
        .unwrap()
        .to_str()
        .unwrap()
        .to_string();

    let bytes = response.bytes().await.unwrap().clone();

    let site_icon = SiteIcon {
        domain,
        content_type,
        bytes,
    };

    save_item(&site_icon);

    site_icon
}
