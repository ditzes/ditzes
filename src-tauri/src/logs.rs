// use env_logger::Builder;
// use log::LevelFilter;
// use std::io::Write;
// use chrono::Local;
use log::LevelFilter;
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Config, Logger, Root};
use log4rs::encode::pattern::PatternEncoder;
use std::env;

// pub fn init() {
//     Builder::new()
//         .format(|buf, record| {
//             writeln!(buf,
//                      "{} [{}] ({}) - {}",
//                      Local::now().format("%Y-%m-%dT%H:%M:%S"),
//                      record.level(),
//                      record.target(),
//                      record.args()
//                     )
//         })
//         .filter(None, LevelFilter::Info)
//         .init();
// }

pub fn init() {
    let stdout = ConsoleAppender::builder().build();

    let mut _config =
        Config::builder().appender(Appender::builder().build("stdout", Box::new(stdout)));

    match env::var("DITZES_LOG_PATH") {
        Ok(key) => {
            let requests = FileAppender::builder()
                //.encoder(Box::new(PatternEncoder::new("{d} - {m}{n}")))
                .encoder(Box::new(PatternEncoder::new("{m}{n}")))
                // .encoder(Box::new(PatternEncoder::new(log_format)))
                .build(key)
                .unwrap();

            _config = _config
                .appender(Appender::builder().build("requests", Box::new(requests)))
                .logger(
                    Logger::builder()
                        .appender("requests")
                        .additive(false)
                        .build("actix_web::middleware::logger", LevelFilter::Info),
                );
        }
        _ => {}
    };

    let config = _config
        .build(Root::builder().appender("stdout").build(LevelFilter::Info))
        .unwrap();

    let _handle = log4rs::init_config(config).unwrap();
}
