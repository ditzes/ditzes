use log::{debug, info, warn};
use std::{sync::Arc, thread, time, time::Instant};
use tokio::{
    self,
    sync::{
        mpsc,
        mpsc::{Receiver, Sender},
        Mutex,
    },
    task::JoinHandle,
};

// 2022-02-15: This was an attempt at making something that can fetch the latest
// N items from HN whenever the application is running and the right settings
// are configured for it. But somehow, somewhere, shit goes wrong and I can't
// for the life of me figure out how to call the async hn::get_latest_items
// inside of the tokio::spawn stuff
//
// So instead of wasting time trying to get this to work properly, I'll just
// make the frontend make a request to the backend to fetch the last N posts
// every X seconds, based on the settings.
//
// That way, we get async calls + concurrency without having to figure out wtf
// "Future is not Send inside tokio::spawn" really means.
//
// So yeah, this file is not really being used anywhere!

#[path = "hn.rs"]
mod hn;

// #[path = "settings.rs"]
// mod settings;
use crate::settings;

#[derive(Debug)]
pub struct UpdateService {
    // If it should be fetching items
    is_enabled: Arc<Mutex<bool>>,
    // How often should the update happen
    update_interval_secs: Arc<Mutex<u64>>,
    // When the latest update happened, for humans
    latest_update_datetime: String,
    // How many items should be fetched each time it updates?
    items_to_fetch: Arc<Mutex<u32>>,
    // Update the lists too? (frontpage, best, new, ask, show, jobs)
    fetch_lists: Arc<Mutex<bool>>,
}

impl UpdateService {
    async fn setup(us: &UpdateService, se: &settings::Settings) {
        let is_enabled = Arc::clone(&us.is_enabled);
        let update_interval_secs = Arc::clone(&us.update_interval_secs);
        let items_to_fetch = Arc::clone(&us.items_to_fetch);
        let fetch_lists = Arc::clone(&us.fetch_lists);
        let settings = se.clone();
        let lists = hn::LISTS.keys();

        tokio::spawn(async move {
            loop {
                let update_interval_secs = update_interval_secs.lock().await;
                thread::sleep(time::Duration::from_secs(*update_interval_secs));
                drop(update_interval_secs);

                let is_enabled = is_enabled.lock().await;
                if *is_enabled {
                    let items_to_fetch = items_to_fetch.lock().await;
                    hn::get_latest_items(true, *items_to_fetch).await;
                    drop(items_to_fetch);
                    let should_fetch_lists = fetch_lists.lock().await;
                    if *should_fetch_lists {
                        for list in lists.clone() {
                            hn::get_list_by_name(settings.clone(), list.to_string())
                                .await
                                .unwrap();
                        }
                    }
                    drop(should_fetch_lists)
                }
                drop(is_enabled);
            }
        });
    }
    pub async fn new(settings: &settings::Settings) -> UpdateService {
        info!("UpdateService#new");
        let us = UpdateService {
            is_enabled: Arc::new(Mutex::new(false)),
            update_interval_secs: Arc::new(Mutex::new(1)),
            latest_update_datetime: "".to_string(),
            items_to_fetch: Arc::new(Mutex::new(100)),
            fetch_lists: Arc::new(Mutex::new(true)),
        };

        UpdateService::setup(&us, settings).await;

        us
    }

    pub async fn new_from_settings(settings: &settings::Settings) -> UpdateService {
        info!("UpdateService#new_from_settings");

        let us = UpdateService {
            is_enabled: Arc::new(Mutex::new(settings.auto_cache)),
            update_interval_secs: Arc::new(Mutex::new(settings.auto_cache_interval)),
            latest_update_datetime: "".to_string(),
            items_to_fetch: Arc::new(Mutex::new(settings.auto_cache_count)),
            fetch_lists: Arc::new(Mutex::new(settings.auto_cache_fetch_lists)),
        };

        UpdateService::setup(&us, settings).await;

        us
    }

    pub async fn start(&self) {
        info!("UpdateService#start");
        let is_enabled = Arc::clone(&self.is_enabled);
        let mut is_enabled = is_enabled.lock().await;
        *is_enabled = true;
    }

    pub async fn stop(&self) {
        info!("UpdateService#stop");
        let is_enabled = Arc::clone(&self.is_enabled);
        let mut is_enabled = is_enabled.lock().await;
        *is_enabled = false;
    }

    pub async fn set_update_interval(&self, new_update_interval: u64) {
        info!("UpdateService#set_update_interval");
        let update_interval_secs = Arc::clone(&self.update_interval_secs);
        let mut update_interval_secs = update_interval_secs.lock().await;
        *update_interval_secs = new_update_interval;
        info!("UpdateService#set_update_interval new update_interval has been set");
    }

    pub fn force_update(&mut self) {}
}
