use serde::{Deserialize, Serialize};
use std::env;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct RustInfo {
    version: String,
    commit: String,
    commit_date: String,
    target: String,
    llvm_version: String,
    channel: String,
    profile: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct GitInfo {
    branch: String,
    commit_timestamp: String,
    semver: String,
    sha: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct BuildInfo {
    rust_info: RustInfo,
    git_info: GitInfo,
}

fn env(env_var: &str) -> String {
    match env::var(env_var) {
        Ok(v) => v,
        _ => "unknown".to_string(),
    }
}

pub fn new() -> BuildInfo {
    let rust_info = RustInfo {
        version: env!("VERGEN_RUSTC_SEMVER").to_string(),
        commit: env!("VERGEN_RUSTC_COMMIT_HASH").to_string(),
        commit_date: env!("VERGEN_RUSTC_COMMIT_DATE").to_string(),
        target: env!("VERGEN_CARGO_TARGET_TRIPLE").to_string(),
        llvm_version: env!("VERGEN_RUSTC_LLVM_VERSION").to_string(),
        channel: env!("VERGEN_RUSTC_CHANNEL").to_string(),
        profile: env!("VERGEN_CARGO_PROFILE").to_string(),
    };

    let git_info = GitInfo {
        branch: env!("VERGEN_GIT_BRANCH").to_string(),
        commit_timestamp: env!("VERGEN_GIT_COMMIT_TIMESTAMP").to_string(),
        semver: env!("VERGEN_GIT_SEMVER").to_string(),
        sha: env!("VERGEN_GIT_SHA").to_string(),
    };

    BuildInfo {
        rust_info,
        git_info,
    }
}

// Rust version
// Cargo version
// Current Commit / Tag
