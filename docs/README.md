Need to come up with a fancy name too.

## Features
- Threaded view like in the good'ol days of forums
	- Meaning that the tree view of comments is gone and instead replaced by a flat list of comments sorted by time instead of score. If the comment is a reply to another comment, it'll contain that comment as a quote + a link to the original comment
- Argue-level for stories: see how much back and forth there is
	- Basically, the story gets a score based on how much back and forward there is between two commentators without other people being involved
- User tags: assign custom tags to users
	- Group users based on X, or assign one Y tag per user, makes it easy to highlight stuff
- discussion-quality: average length of comments
- term-extractor: Point the software towards a thread, and it'll extract all the terms it doesn't recognize. Useful for finding new software, games or terms in a subject-thread. For example, point the term-extractor to a thread about Factorio and you'll end up with a list of similar games. Point it towards a thread about a new library and you'll get a list of alternative, similar ibrary and you'll get a list of alternative, similar libraries.
- offline-first
    - items batch downloaded
    - see edits
    - frontpage cached on the fly
      - any lists really. Be default. a "CachedList" should be used with the current timestamp (date + hour) and optionally can be refreshed. Implemented for all types of lists with items. Refresh can be done on each separate page and via the caching page to caching all the lists at once, plus any children. If readability is activated, caches all the articles as well as the items themselves.
- Cache-management
	- Since things gets cached, there is a management panel
- Optional Readability-mode
	- If you enable this setting, any link you come across will automatically be passed through https://github.com/kumabook/readability as to ensure A) lightweight content, B) caching since first access, C) no 3rd party access to your client-side browser data and finally D) adherence to your preference for dark/light color scheme
	- Optionally-optional setting is to pro-actively re-write links to use 'readability for any links hn-threads come across. It's kind of a destructive action (the software starts to rewrite links for you) so it might mess up some things.
- Optional Search Index
	- could offer local search via https://tantivy-search.github.io/examples/basic_search.html
	- Has a good set of features https://github.com/quickwit-oss/tantivy
- Personal moderation
	- Hide stories containing your keywords
	- Mute people you're tired of hearing, comments and stories
	- Hide comments containing keywords you don't care about
	- Hide comments that have a sentiment score below your custom threshold
		- https://docs.rs/sentiment/latest/sentiment/
	- Hide accounts newer than N years by setting a date which only users who signed up before will show up with their stories or comments
	- Hide comment-threads (will hide every sub-comment to a parent)