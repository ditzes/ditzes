ReadOnlyMode makes it so the application disables all endpoints/functions that can mutate the settings via the API/Tauri commands, and hides various things from the Web UI.

Suitable if you plan on running a public instance that just allows people to browse content without being able to change any settings.