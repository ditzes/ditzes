# Feature - Settings
Settings control how the application works, across multiple levels and abstractions. 

## API Endpoints

### `PUT /api/settings`
> Saves a new Settings map based on JSON values passed as the body. Overwrites the existing Settings always.

### `GET /api/settings`
> Retrieves the currently stored and active Settings map.