# Feature - Search
The search is fully offline, and only works with items you have locally cached.


## API Endpoints

### `POST /api/search/index`
> Builds the search index from the beginning. Removes the existing index before re-indexing all the already cached items

### `GET /api/search/query/{term}`
> Search the existing index for {term} (URLEncoded) and returns the results. Currently limited to top 10 results (according to search score) with no pagination. Will fail and return zero results unless search index has been built at least once before.