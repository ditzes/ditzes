Cached Lists refers to the various lists the HN API exposes, and the caching of those.

Currently, it caches the lists based on the name of the list + the current date + the current hour.

Example filename:  `beststories-2022-02-09-22.bin`