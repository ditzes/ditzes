import http from "k6/http";
import { check, group, sleep } from "k6";
import { Rate } from "k6/metrics";

// A custom metric to track failure rates
var failureRate = new Rate("check_failure_rate");

//const DOMAIN = 'https://ditzes.com'
const DOMAIN = 'http://localhost:8080'

// Options
export let options = {
    discardResponseBodies: true,
    stages: [
        // { target: 10, duration: "10s" },
        // { target: 0, duration: "10s" },
        { target: 2500, duration: "10s" },
        { target: 2500, duration: "10s" },
        // { target: 500, duration: "10s" },
        // { target: 1000, duration: "10s" },
        // { target: 500, duration: "10s" },
        { target: 0, duration: "10s" }
    ],
    thresholds: {
        // We want the 95th percentile of all HTTP request durations to be less than 500ms
        "http_req_duration": ["p(95)<500"],
        // Requests with the staticAsset tag should finish even faster
        "http_req_duration{staticAsset:yes}": ["p(95)<500"],
        // Thresholds based on the custom metric we defined and use to track application failures
        "check_failure_rate": [
            // Global failure rate should be less than 1%
            "rate<0.01",
            // Abort the test early if it climbs over 5%
            { threshold: "rate<=0.05", abortOnFail: true },
        ],
    },
};

export default function () {
    let response = http.get(`${DOMAIN}/frontpage`);

    // check() returns false if any of the specified conditions fail
    let checkRes = check(response, {
        // "http2 is used": (r) => r.proto === "HTTP/2.0",
        "status is 200": (r) => r.status === 200,
        // "content is present": (r) => r.body.indexOf("") !== -1,
    });

    // We reverse the check() result since we want to count the failures
    failureRate.add(!checkRes);

    // Load static assets, all requests
    group("Static Assets", function () {
        // Execute multiple requests in parallel like a browser, to fetch some static resources
        let resps = http.batch([
            ["GET", `${DOMAIN}/css/style.css`, null, { tags: { staticAsset: "yes" } }],
            ["GET", `${DOMAIN}/js/main.js`, null, { tags: { staticAsset: "yes" } }],
            ["GET", `${DOMAIN}/api/settings`, null, { tags: { staticAsset: "no" } }],
            ["GET", `${DOMAIN}/api/list/frontpage`, null, { tags: { staticAsset: "no" } }],
            ["GET", `${DOMAIN}/api/list/frontpage/versions`, null, { tags: { staticAsset: "no" } }],
            ["GET", `${DOMAIN}/api/favicon/about.gitlab.com`, null, { tags: { staticAsset: "no" } }],
            ["GET", `${DOMAIN}/api/favicon/github.com`, null, { tags: { staticAsset: "no" } }],
        ]);
        // Combine check() call with failure tracking
        failureRate.add(!check(resps, {
            "status is 200": (r) => r[0].status === 200 && r[1].status === 200,
            // "reused connection": (r) => r[0].timings.connecting == 0,
        }));
    });

    sleep(Math.random() * 3 + 2); // Random sleep between 2s and 5s
}
