(ns app-state
  (:require
   [reagent.core :as r]))

(defonce app-state
  (r/atom
   {:page :frontpage ;; :comments
    :url "/"
    ;; :loading? true
    :items []}))

