(ns core
  (:require
   [debug :refer [pp pp-str]]
   [reagent.core :as r]
   [reagent.dom :as rd]
   [secretary.core :as secretary :refer-macros [defroute]]
   [goog.events :as events]
   [app-state :refer [app-state]]
   [constants :refer [READONLY_MESSAGE API_URL]]
   [pages.settings :as settings]
   [pages.cache :as cache]
   [pages.build-info :as build-info]
   [pages.about :as about]
   [http-fetch :as http-fetch])
  (:import [goog History]
           [goog.history EventType]))


(def is-in-tauri?
  false)
  ;; (exists? (.-__TAURI__ js/window)))

(defn invoke [cmd-name opts cb]
  (.then
   (.invoke (-> js/window .-__TAURI__) cmd-name (clj->js opts))
   cb))


;; If you add/remove items from here, the max-width media query for #menu
;; needs to change accordingly to change the position of the menu
(def page-items
  [{:key :frontpage
    :title "frontpage"
    :path "/frontpage"
    :api "list/frontpage"}
   {:key :bestpage
    :title "best"
    :path "/best"
    :api "list/bestpage"}
   {:key :newpage
    :title "new"
    :path "/new"
    :api "list/newpage"}
   {:key :askpage
    :title "ask"
    :path "/ask"
    :api "list/askpage"}
   {:key :showpage
    :title "show hn"
    :path "/show"
    :api "list/showpage"}
   {:key :jobspage
    :title "jobs"
    :path "/jobs"
    :api "list/jobspage"}
   {:key :search
    :title "search"
    :path "/search"
    :align :right
    :api "search"}
   {:key :about
    :title "about"
    :align :right
    :path "/about"
    :api "about"}
   {:key :cache
    :title "cache"
    :align :right
    :path "/cache"
    :api "cache"}
   {:key :version
    :title "version"
    :align :right
    :path "/version"
    :api "build_info"}
   {:key :settings
    :title "settings"
    :align :right
    :path "/settings"
    :api "settings"}
   {:key :comments
    :hidden true
    :title "comments"
    :path "/item/:id"
    :api "items"}])

(def routes
  (merge
    ;; "Special" paths
    {"/" [:redirect "/frontpage"]
     "/item" [:fetch-dynamic :comments]}
    ;; Standard ones, taken from page-items
    (reduce
      (fn [acc v]
        (println v)
        (assoc acc (:path v) (:key v)))
      {}
      page-items)))

(defn get-route [full-path]
  (println "full path" full-path)
  (let [splitted-path (rest (clojure.string/split full-path "/"))
        initial-path (str "/" (first splitted-path))]
        ;; filtered-for-params
        ;; (filter
        ;;   (fn [i]
        ;;     (not
        ;;       (clojure.string/includes? i ":")))
        ;;   splitted-path)
        ;; final-key (if (and filtered-for-params (empty? filtered-for-params))
        ;;             full-path
        ;;             filtered-for-params)]
    (let [route (get routes initial-path)]
      (println "route" route)
      (if route
        route
        :not-found))))

(defn read-current-path [win]
  (-> win .-location .-pathname))

;; "/item/:id" + "/item/123" => {:id 123}
(defn matching-params [v1 v2])

(defn get-params [url k]
  (let [matching-path (:path (some #(when (= k (:key %)) %) page-items))
        splitted (clojure.string/split matching-path "/")
        params (map #(if (clojure.string/includes? % ":")
                       (keyword (clojure.string/join "" (rest %)))
                       %)
                    splitted)
        splitted-real-path (clojure.string/split url "/")]
    (reduce
      (fn [acc [k v]]
        (when (keyword? k)
          (assoc acc k v)))
      {}
      (zipmap params splitted-real-path))))

(comment
  (keyword (clojure.string/join "" (rest ":comments")))
  (clojure.string/split "/item/123" "/")
  (get-params "/item/123" :comments))

(defn set-title [new-subtitle]
  (set! (. js/document -title) (str "Ditzes - " new-subtitle)))

(defn set-title-butfirst [new-subtitle]
  (set-title
    (clojure.string/capitalize
      (clojure.string/join
        ""
        (rest
          (clojure.string/split
            new-subtitle
            ""))))))

(defn load-comments [id]
  (println "Loading comments")
  (set-title (str "Loading " id))
  (let  [update-items (fn [comments]
                        (set-title (.-title (first comments)))
                        (swap! app-state
                               assoc
                               :items comments
                               :loading? false))]
    (if is-in-tauri?
      (invoke "get_comments" {:id id} update-items)
      (let [url (str "comments/" id)]
        (http-fetch/http-get url update-items)))))

(defn fetch-dynamic [url page]
  (swap! app-state
         assoc
         :loading? true
         :items []
         :url url
         :page page)
  (let [params (get-params url page)]
    (load-comments (:id params))))

;; Same as go-to but without calling pushState
(defn redirect-to [url]
  (.replaceState js/window.history #js {} "" url)
  (set-title-butfirst url))

(defn set-page-from-path [url]
  (let [route (get-route url)]
    (println "route " route)
    (if (vector? route)
      (condp = (first route)
        :redirect (redirect-to (second route))
        :fetch-dynamic (fetch-dynamic url (second route)))
      (swap! app-state
             assoc
             :url url
             :page route))))

(defn go-to
  ([url]
   ;; (println "Pushing state to URL" url)
   (.pushState js/window.history #js {} "" url)
   (set-title-butfirst url)
   ;; (when-not (= url (-> js/window .-location .-pathname))
   (set-page-from-path url))
  ([ev url]
   (println "Navigating to" url)
   (.preventDefault ev)
   (go-to url)))

;; (declare go-to)
;; 
;; (defn update-internal-url [new-url]
;;   (swap! app-state assoc :url new-url)
;;   (let [route (get-route new-url)]
;;     (if (vector? route)
;;       (condp (first route) =
;;         :redirect (go-to (second route)))
;;       (do
;;         (swap! app-state
;;                assoc
;;                :url new-url
;;                :page route)
;;         (.pushState js/window.history #js {} "" new-url)))))
;; 

(defn handle-popstate [ev]
  ;; (println "Popping state")
  ;; (.log js/console ev)
  (when-not (= (:url @app-state) (read-current-path js/window))
    (set-page-from-path (read-current-path js/window))))
  ;; (when (.-state ev)
  ;;   (let [new-url (-> ev .-state .-url)]
  ;;     (println "Handling popstate, setting new url" new-url)
  ;;     (update-internal-url new-url))))

(defn setup-popstate! [win]
  ;; (println "Setting up listener to popstate")
  (.addEventListener win "popstate" handle-popstate))
  ;; (update-internal-url (-> win .-location .-pathname)))

(defn reset-to-page! [page]
  (swap! app-state
         assoc
         :page page
         :items []
         :loading? true))

(defn load-list-versions [what-list]
  (let [page (some (fn [v]
                    (when (= (:key v) what-list)
                      v))
                  page-items)
        api (str (:api page) "/versions")
        set-list-versions (fn [l]
                            (swap! app-state
                                   assoc
                                   :list-versions
                                   l))]
    (if-not api
      (throw "`api` not found in `page-items`"))
    (http-fetch/http-get api set-list-versions)))

(defn load-list [what-list on-done]
  (let [page (some (fn [v]
                    (when (= (:key v) what-list)
                      v))
                  page-items)
        api (:api page)
        set-items (fn [cached-list]
                    (swap! app-state
                            assoc
                            :items (.-items cached-list)
                            :current-list (.-datetime cached-list)
                            :loading? false)
                    (on-done))]
    (if-not api
      (throw "`api` not found in `page-items`"))
    (reset-to-page! what-list)
    (if is-in-tauri?
      (let [invoke-method (str "get_" api)]
        (invoke invoke-method {} set-items))
      (http-fetch/http-get api set-items))))

(defn load-settings []
  (if is-in-tauri?
    (invoke "get_settings" {} #(swap! app-state
                                      assoc
                                      :settings (js->clj % :keywordize-keys true)))
                                      ;; :loading? false))
    (http-fetch/http-get "settings" #(swap! app-state
                                            assoc
                                            :settings (js->clj % :keywordize-keys true)))))
                                 ;; :loading? false))))

;; (defn go-to
;;   ([url]
;;    (.pushState js/window.history (clj->js {:url url}) url url)
;;    (swap! app-state assoc :url url)
;;    (update-internal-url url))
;;   ([ev url]
;;    (.preventDefault ev)
;;    (go-to url)))

(defn humanize-list-time [t]
  (let [parts (clojure.string/split t "-")
        date (clojure.string/join "-" (butlast parts))
        hour (str (last parts) "h")]
    (str date " " hour)))

(comment
  (humanize-list-time "2022-02-22-23"))
  ;; => 2022-02-22 23h

(defn $list-item [^js/Object item]
  [:div.frontpage-item
   {:title (when (-> @app-state :settings :debug)
             (.stringify js/JSON
                         item
                         ""
                         2))
    :style {:margin-bottom "15px"}}
   [:div
     {:style {:display "flex"
              :justify-content "start"
              :align-items "center"}}
     [:div
       [:img
        {:style {;;:background-color "white"
                 :display "block"
                 :margin-right 10}
         ;; :border "1px solid lightgrey"
         ;; :background-color "lightgrey"
         ;; :border-radius 3}
         :width 32
         :height 32
         :src (str API_URL "/api/favicon/" (or (.-domain item) "news.ycombinator.com"))}]]
         ;; :src (str "https://" (.-domain item) "/favicon.ico")}]
     [:div
       [:a
        {:style {:display "block"
                 :text-decoration "none"}
         :target (when (.-url item) "_blank")
         ;; :onClick #(go-to % (str "/item/" (.-id item)))
         ;; :onClick #(when-not (.-url item)
         ;;             (go-to % (str "/item/" (.-id item))))
         :href (if (.-url item)
                 (.-url item)
                 (str "/item/" (.-id item)))}
        (str (.-title item))
        (when (.-url item)
          [:a
           {:style {:margin-left "7px"
                    :color "grey"}
            :target "_blank"
            :href (str "https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=" (.-domain item) "&sort=byDate&type=story")}
           "("
           (.-domain item)
           ")"])]
       [:a
        {:style {:display "block"
                 :margin-top 3
                 :color "rgb(130, 130, 130)"}
         :href (str "/item/" (.-id item))
         :onClick #(go-to % (str "/item/" (.-id item)))}
        ;; (let [descendants (.-descendants item)
        ;;       comments (if (= descendants))}
        ;; (.log js/console item)
        (str (when-not (= (.-type item) "job")
               (str "Score: " (.-score item) " "))
             (when-not (= (.-type item) "job")
               (str "Comments: " (.-descendants item) " "))
             "By: " (.-by item))]]]])
        ;; Score: 55 Comments: 28 By: gundmc]])

(defn $version-selector []
  [:select
   {:value (:current-list @app-state)
    :onChange (fn [ev]
                (let [new-val (-> ev .-target .-value)
                      new-list-items (.-items (some #(when (= (.-datetime %) new-val) %) (:list-versions @app-state)))]
                  (swap! app-state
                         assoc
                         :current-list new-val
                         :items new-list-items)))}
   (map
     (fn [item]
       [:option
        {:value item}
        "From "
        (humanize-list-time item)])
     (map
       #(.-datetime %)
       (:list-versions @app-state)))])

;;   (load-list :frontpage))
(defn list-view
  [list-key]
  (fn []
    (r/create-class
      {:component-will-mount
       (fn []
         ;; (println "list key" list-key)
         (load-list
           list-key
           (fn []
             (load-list-versions list-key))))
       :component-will-unmount
       (fn []
         (swap! app-state assoc :items []))
       :render
       (fn []
         [:div
          (when (:list-versions @app-state)
            [:div
              {:style {:margin-bottom 5}}
              [$version-selector]])
          (map
            (fn [item]
               ^{:key (.-id item)}
               [$list-item item])
            (:items @app-state))])})))

;; (defn scroll-to [ev id]
;;   (.preventDefault ev)
;;   (let [el (.querySelector
;;              js/document
;;              (str "#item-" id))]
;;     (.scrollIntoView
;;       el
;;       #js {:behavior "smooth"})))
;;   ;; document.querySelector("#item-30265558").scrollIntoView({behavior: "smooth"}))

(defn $comment [^js/Object item]
  [:div.comment
   {:id (str "item-" (.-id item))}
    ;; :style {:margin 10
    ;;         :margin-top -10
    ;;         :padding 20
    ;;         :background-color "rgba(0,0,0,0.1)")}
            ;; :color "black"}}
   ;; [:small (.-id item)]
   (when (.-title item)
     [:h2 (.-title item)])
   (when (.-url item)
     [:div
       "URL: "
       [:a
        {:style {:color "lightblue"}
         :href (.-url item)
         :target "_blank"}
        (.-url item)]])
   (when (.-parent_text item)
     [:div.comment-parent
       ;; {:style {:border "1px solid rgba(0,0,0,0.2)"
       ;;          :margin "0px 20px 20px 20px"
       ;;          :padding 10
       ;;          :color "rgba(0,0,0,0.7)"
       [:blockquote
        {:style {:padding 0
                 :margin 0}
         :dangerouslySetInnerHTML {:__html (.-parent_text item)}}]
       [:div
        {:style {:margin-top 5
                 :font-size 10}}
        [:a
         {:style {:color "lightgrey"}
          ;; :onClick #(scroll-to % (.-parent item))
          :href (str "#item-" (.-parent item))}
         "View"]]])
   [:div.comment-content
    {:dangerouslySetInnerHTML {:__html (.-text item)}}]
   [:div.comment-meta
    {:style {:margin-top "10px"
             :color "lightgrey"}}
    [:small
     "By "
      [:a
       {:href (str "https://news.ycombinator.com/user?id=" (.-by item))
        :target "_blank"
        :style {:color "lightgrey"}}
       (.-by item)]
      " at "
      [:a
       {:href (str "https://news.ycombinator.com/item?id=" (.-id item))
        :target "_blank"
        :style {:color "lightgrey"}}
       (.-human_time item)]]]])
    ;; (when (and (.-parent item)
    ;;            (not= (:url @app-state) (str "/item/" (.-parent item))))]
    ;;   [:div
    ;;    {:style {:float "right"}}
    ;;    [:small
    ;;     [:a
    ;;      {:href (str "/item/" (.-parent item))
    ;;       :style {:color "grey"}}
    ;;      "View parent item"]]]]]])

(defn $comments []
  [:div
   (map (fn [item]
          ^{:key (.-id item)}
          [$comment item])
        (:items @app-state))])

(defn item-text-decoration [page]
  (if (= (:page @app-state) page)
    "underline"
    "none"))

(defn $menu-item [v]
  [:a.menu-link
   {:href (or (:path v) "#")
    :style {:text-decoration (item-text-decoration (:key v))}
    ;; :justify-self (if (= (:align v) :right) "stretch")}
    :onClick #(go-to % (:path v))}
   (if (string? v)
     (str " " v " ")
     (:title v))])

(defn $menu []
  [:div#menu
   [:div#menu-left
     (doall
       (map
         (fn [v]
           ^{:key (:key v)}
           [$menu-item v])
         (interpose " | " (filter #(not= (:align %) :right)
                                  (filter (complement :hidden) page-items)))))]
   [:div#menu-right
     (doall
       (map
         (fn [v]
           ^{:key (:key v)}
           [$menu-item v])
         (interpose " | " (filter #(= (:align %) :right)
                                  (filter (complement :hidden) page-items)))))]])

(defn $not-found []
  [:h1 "Sorry, couldn't find that page..."])

(defn $search []
  (let [read-only? (-> @app-state :settings :read_only_mode)
        search-term (r/atom "hacker news")
        results (r/atom [])
        rebuilding? (r/atom false)
        rebuild-index (fn []
                        (reset! rebuilding? true)
                        (-> (.fetch
                              js/window
                              (str API_URL "/api/search/index")
                              #js{:method "POST"})
                            (.then #(.json %))
                            (.then #(reset! rebuilding? false))))
        do-query (fn [term]
                   (let [encoded-term (.encodeURIComponent
                                        js/window
                                        term)]
                     (-> (.fetch
                           js/window
                           (str API_URL "/api/search/query/" encoded-term))
                         (.then #(.json %))
                         (.then #(reset! results (js->clj % :keywordize-keys true))))))]
    (fn []
      [:div
       [:h3
         "Search"]
       (when read-only?
         [:div
          {:style {:margin-bottom 30}}
          [:strong READONLY_MESSAGE]])
       [:div
         [:input
          {:type "text"
           :onChange #(let [term (-> % .-target .-value)]
                        (reset! search-term term)
                        (do-query term))
           :value @search-term}]]
       [:div
        [:button
         {:onClick #(do-query @search-term)}
         "Query"]]
       [:h4 "Results"]
       [:div
        (doall
          (map
            (fn [res]
              [:div
               {:style {:border "1px solid grey"
                        :margin-bottom 20
                        :padding 5
                        :font-size 11}}
               [:blockquote
                {:style {:margin 0
                         :padding 5}
                 :dangerouslySetInnerHTML {:__html (:text res)}}]
               [:div.comment-meta
                {:style {:color "grey"}}
                [:small
                 "By "
                  [:a
                   {:href (str "https://news.ycombinator.com/user?id=" (:by res))
                    :target "_blank"}
                   (:by res)]
                  " | "
                  [:a
                   {:target "_blank"
                    ;; :href (str "https://news.ycombinator.com/item?id=" (:id res))
                    :href (str "/item/" (:id res))}
                   "View Item"]
                  " | Search Score: "
                  (:score res)]]])
            @results))]
       [:div
        [:pre
          (pp-str @results)]]
       [:div
         {:style {:opacity (if read-only? 0.6 1)}}
         [:h3
          "Rebuild index"]
         [:p "Warning: this operation can be very slow as it re-indexes all the posts"]
         [:button
          {:disabled (or @rebuilding? read-only?)
           :onClick rebuild-index}
          "Rebuild Index"]]])))


(def pages
  {:frontpage (list-view :frontpage)
   :bestpage (list-view :bestpage)
   :newpage (list-view :newpage)
   :askpage (list-view :askpage)
   :showpage (list-view :showpage)
   :jobspage (list-view :jobspage)
   :comments $comments
   :search $search
   :settings settings/$settings
   :about about/$about
   :cache cache/$cache
   :version build-info/$build-info
   :not-found $not-found})

(defn $loading []
  (let [dots (r/atom 0)
        timer (js/setInterval
                (fn []
                  (if (> @dots 2)
                    (reset! dots 0)
                    (swap! dots inc)))
                250)
        _ (swap! dots inc)]
    (r/create-class
      {:component-will-unmount
       (fn []
         ;; (println "Removing interval")
         (js/clearInterval timer))
       :render
       (fn []
        [:div
         {:style {:font-size "2em"
                  :margin 50}}
         "Loading"
         (map (fn [_] ".")
              (range @dots))])})))

(defn $app []
  [:div
   [$menu]
   (when (:loading? @app-state)
     [$loading])
   [:div
     {:style {:margin 10}}
     [(get pages (:page @app-state))]]
     ;; (if (= (:page @app-state) :frontpage)
     ;;   [$frontpage]
     ;;   [$comments])]]
   (when (-> @app-state :settings :debug)
     [:pre
      {:style {:max-width "200px"}}
      (pp-str (js->clj @app-state))])])

;; (defroute "/" []
;;   (go-to "/frontpage"))
;; 
;; (defroute "/frontpage" []
;;   (load-list :frontpage))
;; 
;; (defroute "/best" []
;;   (load-list :bestpage))
;; 
;; (defroute "/new" []
;;   (load-list :newpage))
;; 
;; (defroute "/ask" []
;;   (load-list :askpage))
;; 
;; (defroute "/show" []
;;   (load-list :showpage))
;; 
;; (defroute "/jobs" []
;;   (load-list :jobspage))
;; 
;; #_:clj-kondo/ignore
;; (defroute "/item/:id" [id]
;;   (.log js/console "Loading comments")
;;   (load-comments id))
;; 
;; (defroute "/settings" [id]
;;   (.log js/console "Loading settings page")
;;   (reset-to-page! :settings)
;;   (load-settings))
;; 
;; (defroute "/cache" [id]
;;   (.log js/console "Loading caching page")
;;   (reset-to-page! :cache))

(defn ^:dev/after-load render
  []
  (rd/render [$app]
             (.querySelector
               js/document
               "#app")))

;; (defn dispatch-token! [_ev]
;;   (let [pathname (.-pathname js/window.location)]
;;     (println  "Dispatching token")
;;     (when-not (= pathname (:url @app-state))
;;       (.log js/console "Dispatching token")
;;       (.log js/console pathname)
;;       (secretary/dispatch! pathname)
;;       (swap! app-state assoc :url pathname))))

;; (defonce bind-history
;;   (doto (History.)
;;     (events/listen EventType.NAVIGATE dispatch-token!)
;;     (.setEnabled true)))

(defn run
  []
  (load-settings)
  ;; (set-page-from-current-path js/window)
  (setup-popstate! js/window)
  (go-to (read-current-path js/window))
  ;; (.log js/console "Initial Render")
  ;; (invoke "get_frontpage" {} #(swap! app-state assoc :frontpage %))
  (render))
