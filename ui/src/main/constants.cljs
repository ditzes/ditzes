(ns constants)

(defonce READONLY_MESSAGE "Warning: Currently running in ReadOnly mode, it is not possible to change anything from the Web UI.")

;; (def API_URL "http://localhost:8080")
;; (goog-define VERBOSE false)
(goog-define _API_URL "/set_in_shadow-cljs.edn")

;; Overwrite API_URL when we're in Tauri
(if (exists? (.-__TAURI__ js/window))
  (def API_URL "http://localhost:8080")
  (def API_URL _API_URL))
