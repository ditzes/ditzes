(ns pages.about)

(defn $link
  ([url]
   ($link url url))
  ([title url]
   [:a
    {:href url
     :target "_blank"}
    title]))

(defn $about []
  [:div
   [:h3 "About Ditzes"]
   [:p
    "First off: Welcome to Ditzes!"]
   [:p
    "It was initially built because I just wanted a offline client to read HN comments,
     and then I continued to adding features to it over time."]
   [:p
    "At one point, I came across Tauri and I've always wanted something alternative to Electron,
     so I decided to make Ditzes work with Tauri so I can have a offline application with
     no dependency on the browser at all. State also became easier to manage, as there are no
     limits (besides diskspace) on how much we can save to disk."]
   [:p
    "Here is a rough timeline (without time) on what features were built in what order:"]
   [:ol
    [:li "Basic reader of HN lists + items."]
    [:li "Flat view of comments instead of threaded."]
    [:li "Dark color scheme for the UI."]
    [:li "Caching of the different lists based on date + hour."]
    [:li "Cache management panel."]
    [:li "Found out about Tauri, started rewriting the API/backend in Rust, fighting with making async code happen concurrently and more fun stuff."]
    [:li "Cache management completely changed, everything persisted to disk in users data directory instead of in the browser, added cache visualization and more."]]
   [:h3 "Handy Links"]
   [:div
    [:div
      {:style {:margin-bottom 5}}
      [:strong "Contact: "]
      [$link "contact@ditzes.com" "mailto:contact@ditzes.com"]]
    [:div
      {:style {:margin-bottom 5}}
      [:strong "Source Code (MIT): "]
      [$link "Codeberg.com/ditzes/ditzes" "https://codeberg.org/ditzes/ditzes"]]
    [:div
      {:style {:margin-bottom 5}}
      [:strong "Feature Requests/Report Bugs: "]
      [$link "https://codeberg.org/ditzes/ditzes/issues"]]
    [:div
      {:style {:margin-bottom 5}}
      [:strong "Roadmap: "]
      [$link "https://codeberg.org/ditzes/ditzes/projects/893"]]
    [:div
      {:style {:margin-bottom 5}}
      [:strong "Privacy Policy: "]
      [:span "N/A, no data about visitors is stored"]]
    [:div
      {:style {:margin-bottom 5}}
      [:strong "Terms of Service: "]
      [:span "Don't be a jerk, OSS software provided \"as is\" without warranty of any kind, feel free to fork/copy/modify/publish at will"]]]])
