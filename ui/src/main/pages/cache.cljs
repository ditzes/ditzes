(ns pages.cache
  (:require
    [app-state :refer [app-state]]
    [constants :refer [READONLY_MESSAGE API_URL]]

    [reagent.core :as r]))

(defn $cache []
  ;; Increment `r` to force refresh of image
  (let [read-only? (-> @app-state :settings :read_only_mode)
        r (r/atom 0)
        loading? (r/atom true)
        latest-item-id (r/atom 0)
        latest-items-n (r/atom 10000)
        params (r/atom {:start 0
                        :end 10000})
        fetch-params (r/atom {:start 2500
                              :end 5000})
        auto-refresh-interval (r/atom nil)
        load! (fn []
                (when-not @loading?
                  (reset! loading? true)
                  (swap! r inc)))
        fetch-range (fn [start end]
                     (.then
                       (.fetch js/window
                               (str API_URL "/api/update/" start "/" end)
                               (clj->js
                                 {:method "POST"}))
                       (fn []
                         (swap! r inc))))
        set-latest-n-range (fn [n]
                             (http-fetch/http-get "latest_item_id"
                                       (fn [res]
                                         (let [start (- res @latest-items-n)
                                               end res
                                               p {:start start :end end}]
                                           (reset! latest-item-id res)
                                           (reset! params p)
                                           (reset! fetch-params p)))))]
    (set-latest-n-range 10000)
    (fn []
      [:div
        [:h2
         "Caching Configuration & Debugging"]
        (when read-only?
          [:div [:strong READONLY_MESSAGE]])
        [:div @latest-item-id]
        [:input
         {:value @latest-items-n
          :disabled read-only?
          :onChange #(reset! latest-items-n (-> % .-target .-value))}]
        [:br]
        [:button
         {:onClick set-latest-n-range
          :disabled read-only?}
         (str "Set range to last " @latest-items-n " items")]
        [:div
         "Here you can find all the state around caching, and introspection tools for you to understand what's being stored or not."]
        [:div
         "You can cache specific ranges of items here and inspect the current caching progress."]
        [:div
          [:button
           {:disabled @loading?
            :onClick load!}
           "Refresh"]
          [:button
           {:disabled (or @loading? read-only?)
            :onClick (fn []
                       (if @auto-refresh-interval
                         (do
                           (.clearInterval js/window @auto-refresh-interval)
                           (reset! auto-refresh-interval nil))
                         (reset! auto-refresh-interval
                                 (.setInterval
                                   js/window
                                   load!
                                   1000))))}
           (if @auto-refresh-interval
             "Cancel Auto Refresh"
             "Auto Refresh")]
          [:div
            "View Range"
            [:div
             [:input {:disabled read-only?
                      :onChange #(swap! params assoc :start (-> % .-target .-value))
                      :value (:start @params)}]]
            [:div
             [:input {:disabled read-only?
                      :onChange #(swap! params assoc :end (-> % .-target .-value))
                      :value (:end @params)}]]]
          [:div
            "Fetch Range"
            [:div
             [:input {:disabled read-only?
                      :onChange #(swap! fetch-params assoc :start (-> % .-target .-value))
                      :value (:start @fetch-params)}]]
            [:div
             [:input {:disabled read-only?
                      :onChange #(swap! fetch-params assoc :end (-> % .-target .-value))
                      :value (:end @fetch-params)}]]
            [:button
             {:disabled read-only?
              :onClick #(fetch-range (:start @fetch-params) (:end @fetch-params))}
             "Cache selected fetch range"]
            [:button
             {:disabled read-only?
              :onClick #(swap! fetch-params
                               assoc
                               :start (:start @params)
                               :end (:end @params))}
             "Copy from View Range"]]]
        (let [r-val @r]
          [:div
            [:img
             {:style {:width "100%"}
              :onLoad #(reset! loading? false)
              :src (str
                     API_URL
                     "/api/cache/image_data/"
                     (:start @params)
                     "/"
                     (:end @params)
                     "/image.png?" r-val)}]])])))
