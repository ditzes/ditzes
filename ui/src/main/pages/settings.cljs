(ns pages.settings
  (:require
    [app-state :refer [app-state]]
    [constants :refer [READONLY_MESSAGE API_URL]]))

(defn save-settings! []
  (let [settings-to-save (-> @app-state :settings clj->js js/JSON.stringify)]
    ;; (.log js/console "Saving settings", settings-to-save)
    (->
      (.fetch js/window
              (str API_URL "/api/settings")
              (clj->js
                {:method "PUT"
                 :headers {"Accept" "application/json"
                           "Content-Type" "application/json"}
                 :body settings-to-save}))
      (.then #(.json %))
      (.then (fn [^js/Object res]
               (if (.-is_error res)
                 (.alert js/window (.-message res))
                 (swap! app-state assoc :settings (js->clj res :keywordize-keys true))))))))
               ;; (.log js/console "New settings saved!"))))))

(defn $setting-toggle [read-only? label description k]
  [:div
   {:style {:margin-bottom 10}}
   [:label
     [:strong label]
     ": "
     [:input
      {:type "checkbox"
       :disabled read-only?
       :onChange #(do (swap! app-state assoc-in [:settings k] (-> % .-target .-checked))
                      (save-settings!))
       :value (-> @app-state :settings k)
       :checked (-> @app-state :settings k)}]
     (str (-> @app-state :settings k))
     [:br]
     [:small description]]])

(defn $settings-number [read-only? label description k]
  [:div
   {:style {:margin-bottom 10}}
   [:label
     [:strong label]
     ": "
     [:input
      {:type "number"
       :disabled read-only?
       :onChange #(do (swap! app-state assoc-in [:settings k] (-> % .-target .-value js/parseInt))
                      (save-settings!))
       :value (-> @app-state :settings k)}]
     [:br]
     [:small description]]])

;; pub auto_cache: bool,
;; pub auto_cache_interval: u64,
;; pub auto_cache_count: u32,)

(defn $settings []
  (let [read-only? (-> @app-state :settings :read_only_mode)
        opacity (if read-only? "0.6" "1.0")]
    [:div
     {:style {:max-width 600}}
     [:div
       "Here is a collection of all possible settings Ditzes exposes."
       [:br]
       "Settings are automatically persisted when you change them."]
     (when read-only?
       [:div
        {:style {:margin-top 20
                 :margin-bottom 30}}
        [:strong READONLY_MESSAGE]])
     [:div
      {:style {:margin-top 10
               :opacity opacity}}
      [:pre [:code "Current Settings (on-disk) schema version: " (-> @app-state :settings :version)]]
      [$setting-toggle
       read-only?
       "Dark Mode"
       "If to use darker color scheme or not. It is recommended to use \"Auto Dark Mode\" if possible to avoid flashes of brightness."
       :dark_mode]
      [$setting-toggle
       ;; read-only?
       true
       "Auto Dark Mode"
       "If the application should use OS preferences for dark color schemes or not. This setting is better to use than \"Dark Mode\" as the manual setting might show fast flashing of bright background when the application initializes."
       :auto_dark_mode]
      [$setting-toggle
       read-only?
       "Use Cache"
       "By default, the application caches most content it comes across, to avoid slowness when loading lots of comments. If you have a very, very fast internet connection, you might prefer \"live\" content instead of cached content. If you have a slow internet connection, try leaving this on and activating \"Auto Cache\" instead for a faster browsing experience."
       :use_cache]
      [$setting-toggle
       read-only?
       "Auto Cache"
       "This setting will make Ditzes proactively go out and fetch content and refresh it periodically instead of waiting for you to request it. Use this if you have a internet connection that is not very, very fast, as the HN API requires us to fetch every item individually, which can be very slow for larger threads, even if you have a good internet connection. "
       :auto_cache]
      [$settings-number
       read-only?
       "Auto Cache Interval"
       "How often Auto Cache should see if there is new items to download. Defined in seconds. Default: 60 (once per minute)."
       :auto_cache_interval]
      [$settings-number
       read-only?
       "Auto Cache Fetch Count"
       "How many items should be max fetched each time. Should be a higher number if Auto Cache Interval is higher, as otherwise you might miss to cache items, as Auto Cache just fetched $LATEST_ITEM_ID - $COUNT, so if more items have been submitted to HN since last time Auto Cache ran + $COUNT, those will never be fetched unless you manually fetch them on the /cache page. Default: 1000."
       :auto_cache_count]
      [$setting-toggle
       read-only?
       "Enable Debug Mode"
       "Shows a bunch of useful debug output and data, useful for debugging and seeing the inner workings of Ditzes, it can make things VERY slow or even hang the application as data gets outputted in pretty-printed format to the DOM. Only activate this if you're hacking on Ditzes or if you're a curious individual with a hint of masochism."
       :debug]]]))
