(ns pages.build-info
  (:require
    [debug :refer [pp-str]]

    [app-state :refer [app-state]]
    [constants :refer [READONLY_MESSAGE API_URL]]
    [http-fetch :refer [http-get]]

    [reagent.core :as r]))

(defn $info [k v]
  [:div
   [:strong k]
   ": "
   [:span v]])

(defn $build-info []
  (let [info (r/atom {})]
    (r/create-class
      {:component-will-mount
       (fn []
         (http-get
           "build-info"
           #(reset! info (js->clj % :keywordize-keys true))))
       :render
       (fn []
         [:div
           [:h1 "Version Info"]
           (let [i (:rust_info @info)]
             [:div
               [:h3 "Rust"]
               [$info "Version" (:version i)]
               [$info "Commit" (:commit i)]
               [$info "Commit Date" (:commit_date i)]
               [$info "Channel" (:channel i)]
               [$info "Profile" (:profile i)]
               [$info "Target" (:target i)]
               [$info "LLVM Version" (:llvm_version i)]])
           (let [i (:git_info @info)]
             [:div
               [:h3 "Application Version / Git"]
               [$info "Version" (:semver i)]
               [$info "Branch" (:branch i)]
               [$info "Timestamp" (:commit_timestamp i)]
               [$info "Commit" (:sha i)]])
           (when (:debug? @app-state)
             [:div
              [:pre
               (pp-str (js->clj @info))]])])})))
