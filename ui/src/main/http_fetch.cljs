(ns http-fetch
  (:require
    [constants :refer [API_URL]]))

(defn http-get [path cb]
  (->
    (.fetch
      js/window
      (str API_URL "/api/" path))
    (.then #(.json %))
    (.then cb)))
