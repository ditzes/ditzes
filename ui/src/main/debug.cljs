(ns debug
  (:require
    [cljs.pprint :as pprint]))

;; (goog-define DEBUG true)

(def debug? ^boolean js/goog.DEBUG)

(defn pp [l]
  (when debug?
    (pprint/pprint l)))

(defn pp-str [l]
  (with-out-str (pp l)))
