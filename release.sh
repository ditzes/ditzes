#! /usr/bin/env sh

USER=root
HOST=ditzes.com

log () {
  echo "############"
  echo "#### $1"
  echo "############"
}

log "Building WebUI"
sh -c "cd ui/ && npx shadow-cljs release app"

log "Building API"
sh -c "cd src-tauri/ && cargo build --release"

log "Deploying binaries"
scp src-tauri/target/release/api $USER@$HOST:/root/ditzes-api-new
scp src-tauri/target/release/cli $USER@$HOST:/root/ditzes-cli

# Stop service -> Backup old binary -> Move new binary -> Start service again
log "Restarting service"
ssh $USER@$HOST "systemctl stop ditzes && mv /root/ditzes-api /root/ditzes-api-old && mv /root/ditzes-api-new /root/ditzes-api && systemctl start ditzes && sleep 1 && systemctl status ditzes"

log "Release done!"
