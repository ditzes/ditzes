#! /usr/bin/env bash

set -ex

export COMMIT_MSG="Built from commit $CI_COMMIT_SHA\n$CI_COMMIT_MESSAGE\nFrom author $CI_COMMIT_AUTHOR\n\n$CI_BUILD_LINK"

export RES=$(curl -X 'POST' \
      "https://codeberg.org/api/v1/repos/$CI_REPO/releases" \
      -H 'accept: application/json' \
      -H "Authorization: token $API_TOKEN" \
      -H 'Content-Type: application/json' \
      -d "{
            \"draft\": true,
            \"name\": \"Automated CI release $CI_BUILD_NUMBER\",
            \"tag_name\": \"$CI_COMMIT_SHA\",
            \"body\": \"$COMMIT_MSG\",
            \"prerelease\": true,
            \"target_commitish\": \"$CI_COMMIT_SHA\"
          }")

export RELEASE_ID=$(jq -r '.id' <<< "${RES}")

curl -X 'POST' \
"https://codeberg.org/api/v1/repos/$CI_REPO/releases/$RELEASE_ID/assets" \
-H 'accept: application/json' \
-H "Authorization: token $API_TOKEN" \
-H 'Content-Type: multipart/form-data' \
-F "attachment=@$RELEASE_TAR;type=application/gzip"
